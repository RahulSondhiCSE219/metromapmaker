package djf.ui;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import djf.controller.AppFileController;
import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import java.net.URL;
import djf.AppTemplate;
import static djf.settings.AppStartupConstants.PATH_DATA;
import static djf.settings.AppStartupConstants.PATH_WORK;
import static djf.settings.AppStartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Comparator;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import properties_manager.InvalidXMLFileFormatException;

/**
 * This class provides the basic user interface for this application, including
 * all the file controls, but not including the workspace, which would be
 * customly provided for each app.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class AppGUI {

    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    protected AppFileController fileController;
    protected AppGUI gui;

    // THIS IS THE APPLICATION WINDOW
   public Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    protected Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION AppGUI. NOTE THAT THE WORKSPACE WILL GO
    // IN THE CENTER REGION OF THE appPane
    protected BorderPane appPane;

    // THIS IS THE TOP PANE WHERE WE CAN PUT TOOLBAR
    protected FlowPane topToolbarPane;

    // THIS IS THE FILE TOOLBAR AND ITS CONTROLS
    protected HBox fileToolbar;
    protected HBox copyToolbar;
    protected HBox rightToolbar;
    protected HBox undoToolbar;

    // FILE TOOLBAR BUTTONS
    public Button newButton;
    public Button loadButton;
    public Button saveButton;
    public Button saveAsButton;
    public Button exportButton;
    public Button exitButton;
    public Button redoButton;
    public Button undoButton;
    public Button aboutButton;

    // THIS DIALOG IS USED FOR GIVING FEEDBACK TO THE USER
    protected AppYesNoCancelDialogSingleton yesNoCancelDialog;

    // THIS TITLE WILL GO IN THE TITLE BAR
    protected String appTitle;

    /**
     * This constructor initializes the file toolbar for use.
     *
     * @param initPrimaryStage The window for this application.
     *
     * @param initAppTitle The title of this application, which will appear in
     * the window bar.
     *
     * @param app The app within this gui is used.
     */
    public AppGUI(Stage initPrimaryStage,
            String initAppTitle,
            AppTemplate app) {
        // SAVE THESE FOR LATER
        primaryStage = initPrimaryStage;
        appTitle = initAppTitle;

        // INIT THE TOOLBAR
        initTopToolbar(app);

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow();

        // INIT THE STYLESHEET AND THE STYLE FOR THE FILE TOOLBAR
        initStylesheet(app);
        initFileToolbarStyle();
    }

    /**
     * Accessor method for getting the file toolbar controller.
     */
    public AppFileController getFileController() {
        return fileController;
    }

    /**
     * Accessor method for getting the application pane, within which all user
     * interface controls are ultimately placed.
     *
     * @return This application GUI's app pane.
     */
    public BorderPane getAppPane() {
        return appPane;
    }

    /**
     * Accessor method for getting the toolbar pane in the top, within which
     * other toolbars are placed.
     *
     * @return This application GUI's app pane.
     */
    public FlowPane getTopToolbarPane() {
        return topToolbarPane;
    }

    /**
     * Accessor method for getting the file toolbar pane, within which all file
     * controls are ultimately placed.
     *
     * @return This application GUI's app pane.
     */
    public HBox getFileToolbar() {
        return fileToolbar;
    }

    /**
     * Accessor method for getting this application's primary stage's, scene.
     *
     * @return This application's window's scene.
     */
    public Scene getPrimaryScene() {
        return primaryScene;
    }
    
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * Accessor method for getting this application's window, which is the
     * primary stage within which the full GUI will be placed.
     *
     * @return This application's primary stage (i.e. window).
     */
    public Stage getWindow() {
        return primaryStage;
    }

    /**
     * This method is used to activate/deactivate toolbar buttons when they can
     * and cannot be used so as to provide foolproof design.
     *
     * @param saved Describes whether the loaded Page has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveButton.setDisable(saved);
        saveAsButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        newButton.setDisable(false);
        loadButton.setDisable(false);
        exitButton.setDisable(false);
        exportButton.setDisable(false);
        aboutButton.setDisable(false);
        undoButton.setDisable(false);
        redoButton.setDisable(false);
        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }

    /**
     * *************************************************************************
     */
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR AppGUI */
    /**
     * *************************************************************************
     */
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    public void initTopToolbar(AppTemplate app) {
        fileToolbar = new HBox();
        rightToolbar = new HBox();
        undoToolbar = new HBox();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newButton = initChildButton(fileToolbar, NEW_ICON.toString(), NEW_TOOLTIP.toString(), false);
        loadButton = initChildButton(fileToolbar, LOAD_ICON.toString(), LOAD_TOOLTIP.toString(), false);
        saveButton = initChildButton(fileToolbar, SAVE_ICON.toString(), SAVE_TOOLTIP.toString(), true);
        saveAsButton = initChildButton(fileToolbar, SAVEAS_ICON.toString(), SAVEAS_TOOLTIP.toString(), true);
        exportButton = initChildButton(fileToolbar, EXPORT_ICON.toString(), EXPORT_TOOLTIP.toString(), false);
        exitButton = initChildButton(fileToolbar, EXIT_ICON.toString(), EXIT_TOOLTIP.toString(), false);
        
        aboutButton = initChildButton(rightToolbar, ABOUT_ICON.toString(), ABOUT_TOOLTIP.toString(), false);
        
        undoButton = initChildButton(undoToolbar, UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), true);
        redoButton = initChildButton(undoToolbar, REDO_ICON.toString(), REDO_TOOLTIP.toString(), true);
        
        // AND NOW SETUP THEIR EVENT HANDLERS
        fileController = new AppFileController(app);
        newButton.setOnAction(e -> {
            fileController.handleNewRequest();
        });
        loadButton.setOnAction(e -> {
            fileController.handleLoadRequest();
        });
        saveButton.setOnAction(e -> {
            fileController.handleSaveRequest();
        });
        saveAsButton.setOnAction(e -> {
            fileController.MapNameSaveAs();
        });
        exportButton.setOnAction(e -> {
            fileController.handleExportRequest();
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });

        aboutButton.setOnAction(e -> {
            fileController.handleAboutRequest();
        });

        // NOW PUT THE FILE TOOLBAR IN THE TOP TOOLBAR, WHICH COULD
        // ALSO STORE OTHER TOOLBARS
        topToolbarPane = new FlowPane();
        topToolbarPane.getChildren().add(fileToolbar);
        topToolbarPane.getChildren().add(undoToolbar);
        topToolbarPane.getChildren().add(rightToolbar);
    }

    public boolean loadProperties(String propertiesFileName) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(propertiesFileName, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
        } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
            return false;
        }
    }

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Page IS CREATED OR LOADED
    public void initWindow() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // SET THE WINDOW TITLE
        primaryStage.setTitle(appTitle);

        // START FULL-SCREEN OR NOT, ACCORDING TO PREFERENCES
        primaryStage.setMaximized("true".equals(props.getProperty(START_MAXIMIZED)));

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        appPane = new BorderPane();
        appPane.setTop(topToolbarPane);
        primaryScene = new Scene(appPane);

        // SET THE APP PANE PREFERRED SIZE ACCORDING TO THE PREFERENCES
        double prefWidth = Double.parseDouble(props.getProperty(PREF_WIDTH));
        double prefHeight = Double.parseDouble(props.getProperty(PREF_HEIGHT));
        appPane.setPrefWidth(prefWidth);
        appPane.setPrefHeight(prefHeight);

        // SET THE APP ICON
        String appIcon = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO);
        primaryStage.getIcons().add(new Image(appIcon));

        // NOW TIE THE SCENE TO THE WINDOW
        primaryStage.setScene(primaryScene);
    }

    /**
     * This is a public helper method for initializing a simple button with an
     * icon and tooltip and placing it into a toolbar.
     *
     * @param toolbar Toolbar pane into which to place this button.
     *
     * @param icon Icon image file name for the button.
     *
     * @param tooltip Tooltip to appear when the user mouses over the button.
     *
     * @param disabled true if the button is to start off disabled, false
     * otherwise.
     *
     * @return A constructed, fully initialized button placed into its
     * appropriate pane container.
     */
    public Button initChildButton(Pane toolbar, String icon, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);

        // NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);

        // PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);

        // AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
    public Button initChildButtonText(Pane toolbar, String fill, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // NOW MAKE THE BUTTON
        Button button = new Button(fill);
        button.setDisable(disabled);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);

        // PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);

        // AND RETURN THE COMPLETED BUTTON
        return button;
    }

    /**
     * Note that this is the default style class for the top file toolbar and
     * that style characteristics for this type of component should be put
     * inside app_properties.xml.
     */
    public static final String CLASS_BORDERED_PANE = "bordered_pane";

    /**
     * Note that this is the default style class for the file buttons in the top
     * file toolbar and that style characteristics for this type of component
     * should be put inside app_properties.xml.
     */
    public static final String CLASS_FILE_BUTTON = "file_button";

    /**
     * This function sets up the stylesheet to be used for specifying all style
     * for this application. Note that it does not attach CSS style classes to
     * controls, that must be done separately.
     */
    public void initStylesheet(AppTemplate app) {
        // SELECT THE STYLESHEET
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String stylesheet = props.getProperty(APP_PATH_CSS);
        stylesheet += props.getProperty(APP_CSS);
        Class appClass = app.getClass();
        URL stylesheetURL = appClass.getResource(stylesheet);
        String stylesheetPath = stylesheetURL.toExternalForm();
        primaryScene.getStylesheets().add(stylesheetPath);
    }

    /**
     * This function specifies the CSS style classes for the controls managed by
     * this framework.
     */
    public void initFileToolbarStyle() {
        topToolbarPane.getStyleClass().add(CLASS_BORDERED_PANE);
        fileToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        rightToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        undoToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        newButton.getStyleClass().add(CLASS_FILE_BUTTON);
        loadButton.getStyleClass().add(CLASS_FILE_BUTTON);
        saveButton.getStyleClass().add(CLASS_FILE_BUTTON);
        exitButton.getStyleClass().add(CLASS_FILE_BUTTON);
        aboutButton.getStyleClass().add(CLASS_FILE_BUTTON);
        undoButton.getStyleClass().add(CLASS_FILE_BUTTON);
        redoButton.getStyleClass().add(CLASS_FILE_BUTTON);

        
    }
    
        public void FileSelection() {
        Stage popupwindow = new Stage();

        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("Welcome to Metro Map Maker");

        Label header = new Label("Recent Work:");
        VBox recentlayout = new VBox(20);
        
        File dir = new File(PATH_WORK);
           if(dir.isDirectory()){
            File[] files = dir.listFiles();
            FileFilter fileFilter = new FileFilter()
            {
                @Override
                public boolean accept(File file) {
                    return !file.isDirectory();
                }
            
            };
                files = dir.listFiles(fileFilter);
                Arrays.sort(files, new Comparator(){
                @Override
                    public int compare(Object f1, Object f2){
                        if (((File) f1).lastModified() < ((File) f2).lastModified()){
                            return -1;
                        }else if (((File) f1).lastModified() > ((File) f2).lastModified()){
                            return 1;
                        }else{
                            return 0;
                        }
                    }
               });
                
           int recents = 0;    
           for(File file:files){
            Hyperlink item = new Hyperlink();
            item.setText(file.getName());
            item.setOnAction((ActionEvent e) -> {
                fileController.handleLoadRequest(file.getAbsolutePath());
                popupwindow.close();
            });
                if(recents<5){
                recentlayout.getChildren().add(item);
                }
                recents++;
            }
           
           }
          

        Hyperlink newLink = new Hyperlink();
        newLink.setText("Create New Metro Map");
        newLink.setOnAction((ActionEvent e) -> {
            fileController.handleNewRequest();
            popupwindow.close();
        });
        
        File imageFile = new File("Logo.png");
        Image image = new Image(imageFile.toURI().toString());
        ImageView imageView = new ImageView(image);
        
        VBox Menulayout = new VBox(20);
        BorderPane layout = new BorderPane();
        VBox descLayout = new VBox(60);

        Menulayout.getChildren().addAll(header,recentlayout);
        descLayout.getChildren().addAll(imageView,newLink);
        
        layout.setLeft(Menulayout);
        layout.setRight(descLayout);
        BorderPane.setMargin(Menulayout, new Insets(24,24,24,24));
        BorderPane.setMargin(descLayout, new Insets(24,24,24,24));

        Menulayout.setAlignment(Pos.CENTER);
        descLayout.setAlignment(Pos.CENTER);
        recentlayout.setAlignment(Pos.CENTER);
        header.setAlignment(Pos.CENTER);
        newLink.setAlignment(Pos.CENTER);

        Scene sceneLanguage = new Scene(layout, 700, 416);

        popupwindow.setScene(sceneLanguage);

        popupwindow.showAndWait();
    }
}
