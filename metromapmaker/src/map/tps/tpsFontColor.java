package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.mapData;
import map.gui.mapWorkspace;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import map.data.DraggableText;

/**
 * This class provides a transaction for undoing/redoing
 * changing fill color
 * @author Rahul Sondhi
 */
public class tpsFontColor implements jTPS_Transaction{
    
    Color lastColor;
    Color nextColor;
    AppTemplate app;
    protected AppGUI gui;
    mapData dataManager;
    DraggableText text;
    
    /**
     * Constructor for fill color transaction
     * @param lastColor
     * @param nextColor
     * @param initApp 
     */
    public tpsFontColor(Color lastColor, Color nextColor, DraggableText text, AppTemplate initApp){
        this.lastColor = lastColor;
        this.nextColor = nextColor;
        this.app = initApp;
        this.text = text;
    }

    @Override
    /**
     * Function does a redo of changingFillColor
     */
    public void doTransaction() {
        
        dataManager  = (mapData) app.getDataComponent();
        text.setFill(nextColor);
        app.getGUI().updateToolbarControls(false);
    }

    @Override
        /**
     * Function does a undo of changingFillColor
     */
    public void undoTransaction() {
        dataManager = (mapData) app.getDataComponent();
        text.setFill(lastColor);
        app.getGUI().updateToolbarControls(false);
    }
    
    
    
    
    
}
