/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import map.data.DraggableStation;
import map.data.mapData;
import map.gui.mapWorkspace;

/**
 *
 * @author hooligan
 */
public class tpsMoveLabel implements jTPS_Transaction {

    int prev;
    int selected;
    DraggableStation stat;
    AppTemplate app;
    protected AppGUI gui;
    mapData dataManager;

    public tpsMoveLabel(AppTemplate app, int prev, int selected, DraggableStation stat) {
        this.app = app;
        this.stat = stat;
        this.prev = prev;
        this.selected = selected;
    }

    @Override
    /**
     * Function does a redo of editing a line
     */
    public void doTransaction() {
        stat.labelState = selected;
        stat.labelText.xProperty().unbind();
        stat.labelText.yProperty().unbind();
        switch (stat.labelState) {
            case 2:
                stat.labelText.xProperty().bind(stat.centerXProperty().add(-20.00));
                stat.labelText.yProperty().bind(stat.centerYProperty().add(20.00));
                break;
            case 3:
                stat.labelText.xProperty().bind(stat.centerXProperty().add(-20.00));
                stat.labelText.yProperty().bind(stat.centerYProperty().add(-20.00));
                break;
            case 4:
                stat.labelText.xProperty().bind(stat.centerXProperty().add(20.00));
                stat.labelText.yProperty().bind(stat.centerYProperty().add(-20.00));
                break;
            case 1:
                stat.labelText.xProperty().bind(stat.centerXProperty().add(20.00));
                stat.labelText.yProperty().bind(stat.centerYProperty().add(20.00));
                break;
        }
    }

    @Override
    /**
     * Function does a redo of editing a line
     */
    public void undoTransaction() {
                stat.labelState = prev;
        stat.labelText.xProperty().unbind();
        stat.labelText.yProperty().unbind();
        switch (stat.labelState) {
            case 2:
                stat.labelText.xProperty().bind(stat.centerXProperty().add(-20.00));
                stat.labelText.yProperty().bind(stat.centerYProperty().add(20.00));
                break;
            case 3:
                stat.labelText.xProperty().bind(stat.centerXProperty().add(-20.00));
                stat.labelText.yProperty().bind(stat.centerYProperty().add(-20.00));
                break;
            case 4:
                stat.labelText.xProperty().bind(stat.centerXProperty().add(20.00));
                stat.labelText.yProperty().bind(stat.centerYProperty().add(-20.00));
                break;
            case 1:
                stat.labelText.xProperty().bind(stat.centerXProperty().add(20.00));
                stat.labelText.yProperty().bind(stat.centerYProperty().add(20.00));
                break;
        }

    }

}
