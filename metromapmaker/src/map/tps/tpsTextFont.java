package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.DraggableText;
import map.gui.mapWorkspace;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;

/**
 * This class provides a transaction for undoing/redoing
 * setting text font
 * @author Rahul Sondhi
 */
public class tpsTextFont implements jTPS_Transaction{
    
    DraggableText text;
    AppTemplate app;
    String lastFamily;
    String nextFamily;
    protected AppGUI gui;
    
    /**
     * Constructor for text font transaction
     * @param lastFamily
     * @param nextFamily
     * @param initApp
     * @param text 
     */
    public tpsTextFont(String lastFamily, String nextFamily, AppTemplate initApp, DraggableText text){
        this.lastFamily = lastFamily;
        this.nextFamily = nextFamily;
        this.app = initApp;
        this.text = text;
    }

    @Override
    /**
     * Function does a redo of setting text font
     */
    public void doTransaction() {   
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(text);
        text.setFont(Font.font(nextFamily, FontWeight.NORMAL, FontPosture.REGULAR, text.getFont().getSize()));
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(text);
        app.getGUI().updateToolbarControls(false);     
    }

    @Override
     /**
     * Function does a undo of setting text font
     */
    public void undoTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(text);
        text.setFont(Font.font(lastFamily, FontWeight.NORMAL, FontPosture.REGULAR, text.getFont().getSize()));
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(text);
        app.getGUI().updateToolbarControls(false);
    }
    
    
    
}
