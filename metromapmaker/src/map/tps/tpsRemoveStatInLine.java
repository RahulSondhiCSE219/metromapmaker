/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import jtps.jTPS_Transaction;
import map.data.DraggableLine;
import map.data.DraggableStation;
import map.data.DraggableText;
import map.gui.mapWorkspace;

/**
 *
 * @author hooligan
 */
public class tpsRemoveStatInLine implements jTPS_Transaction {

    DraggableLine line;
    DraggableStation stat;
    AppTemplate app;
    protected AppGUI gui;
    
    /**
     * Constructor for adding transaction
     * @param initApp
     * @param allShape 
     */
    public tpsRemoveStatInLine(AppTemplate initApp, DraggableLine allShape, DraggableStation stat) {
        this.line = allShape;
        app =initApp;
        this.stat = stat;
    }
    
    @Override
     /**
     * Functions does the redo of adding a shape
     */
    public void doTransaction() {
       line.removeStations(stat);
    }

    @Override
     /**
     * Function does the undo of adding a shape
     */
    public void undoTransaction() {        
        line.addStations(stat);
    }
    
}
