package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.mapData;
import map.gui.mapWorkspace;
import jtps.jTPS_Transaction;
import map.data.DraggableLine;

/**
 * This class provides a transaction for undoing/redoing
 * changing the line thickness of an object
 * @author Rahul Sondhi
 */
public class tpsLineThickness implements jTPS_Transaction{
    
    mapData dataManager;
    double lastThickness;
    double nextThickness;
    AppTemplate app;
    DraggableLine line;
    protected AppGUI gui;
    
    /**
     * Constructor for line thickness transaction
     * @param lastThickness
     * @param nextThickness
     * @param initApp 
     */
    public tpsLineThickness(double lastThickness, double nextThickness, DraggableLine line, AppTemplate initApp){
        this.lastThickness = lastThickness;
        this.nextThickness = nextThickness;
        this.app = initApp;
        this.line = line;
    }

    @Override
     /**
     * Function does a redo of changing the line thickness of an object
     */
    public void doTransaction() {
        dataManager = (mapData) app.getDataComponent();
        line.setStrokeWidth(nextThickness);
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());
        workspace.getLineThicknessSlider().setValue(nextThickness);
    }

    @Override
         /**
     * Function does a undo of changing the line thickness of an object
     */
    public void undoTransaction() {
        dataManager = (mapData) app.getDataComponent();
        line.setStrokeWidth(lastThickness);
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());
        workspace.getLineThicknessSlider().setValue(lastThickness);
    }
    
}
