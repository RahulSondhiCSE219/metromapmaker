/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing
 * zooming out
 * @author Rahul Sondhi
 */
public class tpsZoomOut  implements jTPS_Transaction{
    
    AppTemplate app;
    double prev;
    double current;
    
    /**
     * Constructor for zoom out transaction
     */
    public tpsZoomOut(AppTemplate initApp,double prev,double current){
        app =initApp;
        this.prev = prev;
        this.current = current;
    }
    
    @Override
    /**
     * Function does a redo of zooming out
     */
    public void doTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().setScaleX(current);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().setScaleY(current);
    }
    
    @Override
    /**
     * Function does a undo of zooming out
     */
    public void undoTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().setScaleX(prev);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().setScaleY(prev);
    }
    
}
