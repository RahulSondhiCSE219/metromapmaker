package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.DraggableText;
import map.gui.mapWorkspace;

import jtps.jTPS_Transaction;

/**
 * This class provides a transaction for undoing/redoing
 * setting text
 * @author Rahul Sondhi
 */
public class tpsText implements jTPS_Transaction{
    
    DraggableText text;
    AppTemplate app;
    protected AppGUI gui;
    String lastString;
    String nextString;
    
    /**
     * Constructor for text transaction
     * @param lastString
     * @param nextString
     * @param initApp
     * @param text 
     */
    public tpsText(String lastString, String nextString, AppTemplate initApp, DraggableText text){
        this.lastString = lastString;
        this.nextString = nextString;
        this.app = initApp;
        this.text = text;
    }

    @Override
     /**
     * Function does a redo of setting text
     */
    public void doTransaction() {
        ((mapWorkspace)app.getWorkspaceComponent()).getCanvas().getChildren().remove(text);
        text.setText(nextString);
        ((mapWorkspace)app.getWorkspaceComponent()).getCanvas().getChildren().add(text);
    }

    @Override
     /**
     * Function does a undo of setting text
     */
    public void undoTransaction() {
        ((mapWorkspace)app.getWorkspaceComponent()).getCanvas().getChildren().remove(text);
        text.setText(lastString);
        ((mapWorkspace)app.getWorkspaceComponent()).getCanvas().getChildren().add(text);
    }
    
    
    
    
    
    
}



    
