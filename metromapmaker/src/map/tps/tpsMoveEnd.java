/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import jtps.jTPS_Transaction;

/**
 * This class provides a transaction for undoing/redoing
 * MoveEnd
 * @author Rahul Sondhi
 */
public class tpsMoveEnd  implements jTPS_Transaction{
    
    /**
     * Constructor for MoveEnd Transaction
     */
    public tpsMoveEnd(){

    }
    
    @Override
    /**
     * Function does a redo of MoveEnd
     */
    public void doTransaction() {
        
    }
    
    @Override
    /**
     * Function does a undo of MoveEnd
     */
    public void undoTransaction() {
        
    }
    
}
