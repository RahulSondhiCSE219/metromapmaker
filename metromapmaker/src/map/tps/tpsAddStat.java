package map.tps;

import jtps.jTPS_Transaction;
import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.scene.Node;
import map.data.DraggableLine;
import map.data.DraggableStation;
import map.data.DraggableText;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing
 * adding of objects
 *
 * @author Rahul Sondhi
 */
public class tpsAddStat implements jTPS_Transaction {

    DraggableStation allShape;
    DraggableText start;
    AppTemplate app;
    protected AppGUI gui;

    /**
     * Constructor for adding transaction
     * @param initApp
     * @param allShape 
     */
    public tpsAddStat(AppTemplate initApp, DraggableStation allShape, DraggableText start) {
        this.allShape = allShape;
        app =initApp;
        this.start = start;
    }
    
    @Override
     /**
     * Functions does the redo of adding a shape
     */
    public void undoTransaction() {
       ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(allShape);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(start);
        ((mapWorkspace) app.getWorkspaceComponent()).destStatName.remove(allShape.name);
        ((mapWorkspace) app.getWorkspaceComponent()).originStatName.remove(allShape.name);
        ((mapWorkspace) app.getWorkspaceComponent()).statNames.remove(allShape.name);
    }

    @Override
     /**
     * Function does the undo of adding a shape
     */
        public void doTransaction() {        
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(allShape);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(start);
        ((mapWorkspace) app.getWorkspaceComponent()).destStatName.add(allShape.name);
        ((mapWorkspace) app.getWorkspaceComponent()).originStatName.add(allShape.name);
        ((mapWorkspace) app.getWorkspaceComponent()).statNames.add(allShape.name);
    }

}
