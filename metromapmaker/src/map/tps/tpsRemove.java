package map.tps;

import jtps.jTPS_Transaction;
import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.scene.Node;
import javafx.scene.shape.Shape;
import map.data.mapData;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing
 * removing of objects
 * @author Rahul Sondhi
 */
public class tpsRemove implements jTPS_Transaction{

    Shape thisShape;
    AppTemplate app;
    AppGUI gui;
    mapData dataManager;
    
    /**
     * Constructor for removing transaction
     * @param initApp
     * @param thisShape 
     */
    public tpsRemove(AppTemplate initApp, Shape thisShape){
        this.thisShape = thisShape;
        app = initApp;
        dataManager = (mapData) app.getDataComponent();
    }
    
 
     @Override
     /**
     * Function does a redo of removing a shape
     */
    public void doTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(thisShape);
    }

    @Override
    /**
     * Function does an undo of removing a shape
     */
    public void undoTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(thisShape);
    }

}
