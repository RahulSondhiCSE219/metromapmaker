/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import jtps.jTPS_Transaction;
import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.scene.paint.Color;
import map.data.DraggableLine;
import map.data.mapData;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing
 * editing a line
 * @author Rahul Sondhi
 */
public class tpsEditLine implements jTPS_Transaction{
    
    String prevName;
    Color prevColor; 
    String text; 
    Color selectedColor; 
    DraggableLine line;
    AppTemplate app;
    protected AppGUI gui;
    mapData dataManager;
    
    /**
     * Constructor for Edit Line Transaction
     */
    public tpsEditLine(AppTemplate app, String prevName, Color prevColor, String text, Color selectedColor, DraggableLine selectedLine) {
        this.app = app;
        line = selectedLine;
        this.prevColor = prevColor;
        this.prevName = prevName;
        this.text = text;
        this.selectedColor = selectedColor;
    }
    
    @Override
    /**
     * Function does a redo of editing a line
     */
    public void doTransaction() {
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());
        dataManager = (mapData) app.getDataComponent();
        
        workspace.lineNames.remove(prevName);
        line.name = text;
        line.endText.setText(text);
        line.startText.setText(text);
        line.endText.setStroke(selectedColor);
        line.startText.setStroke(selectedColor);
        line.endText.setFill(selectedColor);
        line.startText.setFill(selectedColor);
        line.setStroke(selectedColor);
        workspace.lineNames.add(text);
    }
    
    @Override
    /**
     * Function does a redo of editing a line
     */
    public void undoTransaction() {
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());
        dataManager = (mapData) app.getDataComponent();
        
        workspace.lineNames.remove(text);
        line.name = prevName;
        line.endText.setText(prevName);
        line.startText.setText(prevName);
        line.endText.setStroke(prevColor);
        line.startText.setStroke(prevColor);
        line.endText.setFill(prevColor);
        line.startText.setFill(prevColor);
        line.setStroke(prevColor);
        workspace.lineNames.add(prevName);
        
    }
}
