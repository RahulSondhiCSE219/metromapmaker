package map.tps;

import jtps.jTPS_Transaction;
import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.scene.Node;
import map.data.DraggableLine;
import map.data.DraggableText;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing
 * adding of objects
 *
 * @author Rahul Sondhi
 */
public class tpsAddLine implements jTPS_Transaction {

    DraggableLine allShape;
    DraggableText start;
    DraggableText end;
    AppTemplate app;
    protected AppGUI gui;

    /**
     * Constructor for adding transaction
     * @param initApp
     * @param allShape 
     */
    public tpsAddLine(AppTemplate initApp, DraggableLine allShape, DraggableText start, DraggableText end) {
        this.allShape = allShape;
        app =initApp;
        this.start = start;
        this.end = end;
    }
    
    @Override
     /**
     * Functions does the redo of adding a shape
     */
    public void doTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(allShape);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(start);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(end);
        ((mapWorkspace) app.getWorkspaceComponent()).lineNames.add(allShape.name);
    }

    @Override
     /**
     * Function does the undo of adding a shape
     */
        public void undoTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(allShape);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(start);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(end);
        ((mapWorkspace) app.getWorkspaceComponent()).lineNames.remove(allShape.name);
    }

}
