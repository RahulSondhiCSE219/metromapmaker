/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import jtps.jTPS_Transaction;
import map.data.DraggableStation;
import map.data.mapData;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing
 * toggling snap
 * @author Rahul Sondhi
 */
public class tpsToggleSnap  implements jTPS_Transaction{
    double prevX;
    double prevY;
    double currentX;
    double currentY;
    DraggableStation station;
        AppTemplate app;
    protected AppGUI gui;
    mapData dataManager;
    /**
     * Constructor for snap to grid transaction
     */
    public tpsToggleSnap(double prevX, double prevY,double currentX,double currentY, DraggableStation station, AppTemplate initApp){
        this.prevX = prevX;
        this.prevY = prevY;
        this.currentX = currentX;
        this.currentY = currentY;
        this.app = initApp;
        this.station = station;
    }
    
    @Override
    /**
     * Function does a redo of toggle snap to grid
     */
    public void doTransaction() {
        dataManager = (mapData) app.getDataComponent();
        station.setCenterX(currentX);
        station.setCenterY(currentY);
    }
    
    @Override
    /**
     * Function does a undo of toggle snap to grid
     */
    public void undoTransaction() {
        dataManager = (mapData) app.getDataComponent();
        station.setCenterX(prevX);
        station.setCenterY(prevY);
    }
    
}
