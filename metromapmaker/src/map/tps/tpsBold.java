package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.DraggableText;
import map.gui.mapWorkspace;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;

public class tpsBold implements jTPS_Transaction{
    
    DraggableText tempText;
    protected AppGUI gui;
    AppTemplate initApp;
    
    
    public tpsBold(DraggableText tempText, AppTemplate initApp){
        this.tempText = tempText;
        this.initApp = initApp; 
    }

    @Override
    public void doTransaction() {
        tempText.setFont(Font.font(tempText.getFont().getFamily(), FontWeight.BOLD, tempText.getFont().getSize()));
    }

    @Override
    public void undoTransaction() {
            tempText.setFont(Font.font(tempText.getFont().getFamily(), FontWeight.NORMAL, tempText.getFont().getSize()));
    }
    
    
    
}
