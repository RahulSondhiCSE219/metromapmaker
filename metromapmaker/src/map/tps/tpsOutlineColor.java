package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.mapData;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 * This class provides a transaction for undoing/redoing
 * outline color of a shape
 * @author Rahul Sondhi
 */
public class tpsOutlineColor implements jTPS_Transaction {

    AppTemplate app;
    mapData dataManager;

    Color previousColor;
    Color currentColor;
    protected AppGUI gui;
    
     /**
     * Constructor for outline color transaction
     * @param previousColor
     * @param currentColor
     * @param initApp
     */
    public tpsOutlineColor(Color previousColor, Color currentColor, AppTemplate initApp) {
        this.app = initApp;
        this.previousColor = previousColor;
        this.currentColor = currentColor;
    }

    @Override
     /**
     * Function does a redo of setting an outline color of a shape
     */
    public void doTransaction() {
        dataManager = (mapData) app.getDataComponent();
        dataManager.setCurrentOutlineColor(currentColor);
    }

    @Override
     /**
     * Function does a undo of setting an outline color of a shape
     */
    public void undoTransaction() {
        dataManager = (mapData) app.getDataComponent();
        dataManager.setCurrentOutlineColor(previousColor);
    }

}
