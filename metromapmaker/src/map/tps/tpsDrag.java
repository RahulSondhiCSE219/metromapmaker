package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.Draggable;
import map.data.DraggableEllipse;
import map.data.DraggableRectangle;
import map.data.DraggableText;
import map.gui.mapWorkspace;
import javafx.scene.Node;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 * This class provides a transaction for undoing/redoing
 * dragging of objects
 * @author Rahul Sondhi
 */
public class tpsDrag implements jTPS_Transaction{
    
    double oldX;
    double oldY; 
    double newX;
    double newY;
    protected AppGUI gui;
    DraggableRectangle tempRectangle;
    DraggableText tempText;
    DraggableEllipse tempEllipse;
    
    
    Draggable tempShape;
    
    /**
     * Constructor for dragging transaction
     * @param oldX
     * @param oldY
     * @param newX
     * @param newY
     * @param tempShape
     * @param app 
     */
    public tpsDrag(double oldX, double oldY, double newX, double newY, Draggable tempShape, AppTemplate app){
        this.oldX = oldX;
        this.oldY = oldY;
        this.newX = newX;
        this.newY = newY;
        this.tempShape = tempShape;
    }

    @Override
    /**
     * Function does a redo of dragging a shape
     */
    public void doTransaction() {
        if(tempShape instanceof DraggableText){
            tempText = (DraggableText) tempShape;
            tempText.setX(newX);
            tempText.setY(newY);
        }else if(tempShape instanceof DraggableEllipse){
            tempEllipse = (DraggableEllipse) tempShape;
            tempEllipse.setCenterX(newX);
            tempEllipse.setCenterY(newY);

        }else if (tempShape instanceof DraggableRectangle){
            tempRectangle = (DraggableRectangle) tempShape;
            tempRectangle.setX(newX);
            tempRectangle.setY(newY);
        }
    }

    @Override
     /**
     * Function does a redo of dragging a shape
     */
    public void undoTransaction() {
         if(tempShape instanceof DraggableText){
            tempText = (DraggableText) tempShape;
            tempText.setX(oldX);
            tempText.setY(oldX);
        }else if(tempShape instanceof DraggableEllipse){ 
            tempEllipse = (DraggableEllipse) tempShape;
            tempEllipse.setCenterX(oldX);
            tempEllipse.setCenterY(oldX);

        }else if (tempShape instanceof DraggableRectangle){            
             tempRectangle = (DraggableRectangle) tempShape;
            tempRectangle.setX(newX);
            tempRectangle.setY(newY);
        }
    }
    
}
