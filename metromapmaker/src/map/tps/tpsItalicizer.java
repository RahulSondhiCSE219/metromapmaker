package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.DraggableText;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import jtps.jTPS_Transaction;

public class tpsItalicizer implements jTPS_Transaction{
    
    DraggableText text;
    AppTemplate initApp;
    protected AppGUI gui;
    
    public tpsItalicizer(DraggableText text, AppTemplate initApp){
        this.text = text;
        this.initApp = initApp;
    }

    @Override
    public void doTransaction() {
        text.setFont(Font.font(text.getFont().getFamily(), FontPosture.ITALIC, text.getFont().getSize()));
    }

    @Override
    public void undoTransaction() {
            text.setFont(Font.font(text.getFont().getFamily(), FontPosture.REGULAR, text.getFont().getSize()));
    }  
    
}
