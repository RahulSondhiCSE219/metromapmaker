/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing
 * decreasing map size
 * @author Rahul Sondhi
 */
public class tpsDecreaseMapSize implements jTPS_Transaction{
    
    AppTemplate app;
    double prevX;
    double prevY;
    double currentX;
    double currentY;
    
    /**
     * Constructor for decrease map size transaction
     */
    public tpsDecreaseMapSize(AppTemplate initApp,double prevX, double prevY,double currentX,double currentY){
        app = initApp;
        this.prevX = prevX;
        this.prevY = prevY;
        this.currentX = currentX;
        this.currentY = currentY;
    }
    
    @Override
    /**
     * Function does a redo of decreasing map size
     */
    public void doTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().setPrefWidth(currentX);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().setPrefHeight(currentY);
    }
    
    @Override
    /**
     * Function does a undo of decreasing map size
     */
    public void undoTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().setPrefWidth(prevX);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().setPrefHeight(prevY);
    }
    
    
}
