package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.DraggableText;
import map.gui.mapWorkspace;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;

/**
 * This class provides a transaction for undoing/redoing
 * setting text size
 * @author Rahul Sondhi
 */
public class tpsTextSize implements jTPS_Transaction {

    DraggableText text;
    AppTemplate app;
    double lastSize;
    double nextSize;
    protected AppGUI gui;
    
    /**
     * Constructor for text size transaction
     * @param lastSize
     * @param nextStr
     * @param initApp
     * @param text 
     */
    public tpsTextSize(double lastSize, double nextStr, AppTemplate initApp, DraggableText text) {
        this.lastSize = lastSize;
        this.nextSize = nextStr;
        this.app = initApp;
        this.text = text;
    }

    @Override
     /**
     * Function does a redo of setting text size
     */
    public void doTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(text);
        text.setFont(Font.font(text.getFont().getFamily(), FontWeight.NORMAL, FontPosture.REGULAR, nextSize));
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(text);
    }

    @Override
     /**
     * Function does a redo of setting text size
     */
    public void undoTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(text);
        text.setFont(Font.font(text.getFont().getFamily(), FontWeight.NORMAL, FontPosture.REGULAR, lastSize));
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(text);
    }

}
