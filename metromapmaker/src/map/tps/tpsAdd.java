package map.tps;

import jtps.jTPS_Transaction;
import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.scene.Node;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing
 * adding of objects
 *
 * @author Rahul Sondhi
 */
public class tpsAdd implements jTPS_Transaction {

    Node allShape;
    AppTemplate app;
    protected AppGUI gui;

    /**
     * Constructor for adding transaction
     * @param initApp
     * @param allShape 
     */
    public tpsAdd(AppTemplate initApp, Node allShape) {
        this.allShape = allShape;
        app =initApp;     
    }
    
    @Override
     /**
     * Functions does the redo of adding a shape
     */
    public void doTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(allShape);
    }

    @Override
     /**
     * Function does the undo of adding a shape
     */
        public void undoTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(allShape);
    }

}
