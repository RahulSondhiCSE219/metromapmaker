/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import jtps.jTPS_Transaction;
import map.data.DraggableStation;
import map.data.mapData;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing
 * rotating an object
 * @author Rahul Sondhi
 */
public class tpsRotate  implements jTPS_Transaction{
        mapData dataManager;
    double prevRotate;
    double currentRotate;
    DraggableStation station;
    AppTemplate app;
    protected AppGUI gui;
    /**
     * Constructor for rotate transaction
     */
    public tpsRotate(double prevRotate, double currentRotate, DraggableStation station, AppTemplate initApp){
        this.prevRotate = prevRotate;
        this.currentRotate = currentRotate;
        this.app = initApp;
        this.station = station;
    }
    
    @Override
    /**
     * Function does a redo of rotating an object
     */
    public void doTransaction() {
        dataManager = (mapData) app.getDataComponent();
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());
        station.labelText.setRotate(currentRotate); 
        station.rotate = currentRotate;
    }
    
    @Override
    /**
     * Function does a undo of rotating an object
     */
    public void undoTransaction() {
        dataManager = (mapData) app.getDataComponent();
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());
        station.labelText.setRotate(prevRotate);
        station.rotate = prevRotate;
    }
    
}
