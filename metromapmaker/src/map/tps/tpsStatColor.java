/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import map.data.DraggableLine;
import map.data.DraggableStation;
import map.data.mapData;
import map.gui.mapWorkspace;

/**
 *
 * @author hooligan
 */
public class tpsStatColor implements jTPS_Transaction{
    
    Color prevColor; 
    Color selectedColor; 
    DraggableStation stat;
    AppTemplate app;
    protected AppGUI gui;
    mapData dataManager;
    
    public tpsStatColor(AppTemplate app, Color prevColor, Color selectedColor, DraggableStation stat) {
        this.app = app;
        this.stat = stat;
        this.prevColor = prevColor;
        this.selectedColor = selectedColor;
    }
    
    @Override
    /**
     * Function does a redo of editing a line
     */
    public void doTransaction() {
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());
        dataManager = (mapData) app.getDataComponent();
        
        stat.labelText.setStroke(selectedColor);
        stat.labelText.setFill(selectedColor);
        stat.setStroke(selectedColor);
        stat.setFill(selectedColor);
    }
    
    @Override
    /**
     * Function does a redo of editing a line
     */
    public void undoTransaction() {
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());
        dataManager = (mapData) app.getDataComponent();
        
        stat.labelText.setStroke(prevColor);
        stat.labelText.setFill(prevColor);
        stat.setStroke(prevColor);
        stat.setFill(prevColor);
        
    }
    
}
