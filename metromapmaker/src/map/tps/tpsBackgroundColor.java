package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.mapData;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 * This class provides a transaction for undoing/redoing
 * setting a background color
 * @author Rahul Sondhi
 */
public class tpsBackgroundColor implements jTPS_Transaction{
    
    Color prevColor, curColor;
    AppTemplate app;
    mapData dataManager;
    protected AppGUI gui;
    
    /**
     * Constructor for background color transaction
     * @param prevColor
     * @param curColor
     * @param initApp 
     */
    public tpsBackgroundColor(Color prevColor, Color curColor, AppTemplate initApp){
        this.prevColor = prevColor;
        this.curColor = curColor;
        this.app = initApp;
    }

    @Override
     /**
     * Function does a redo of rotating an object
     */
    public void doTransaction() {
        dataManager = (mapData) app.getDataComponent();
        dataManager.setBackgroundColor(curColor);
    }

    @Override
     /**
     * Function does a undo of rotating an object
     */
    public void undoTransaction() {
        dataManager = (mapData)app.getDataComponent();
        dataManager.setBackgroundColor(prevColor);
    }
    
}
