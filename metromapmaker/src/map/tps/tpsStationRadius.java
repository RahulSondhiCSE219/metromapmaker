/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import jtps.jTPS_Transaction;
import map.data.DraggableStation;
import map.data.mapData;
import map.gui.mapWorkspace;

/**
 *
 * @author hooligan
 */
public class tpsStationRadius implements jTPS_Transaction{
    
    mapData dataManager;
    double lastThickness;
    double nextThickness;
    DraggableStation station;
    AppTemplate app;
    protected AppGUI gui;
    
    /**
     * Constructor for station radius
     * @param lastThickness
     * @param nextThickness
     * @param initApp 
     */
    public tpsStationRadius(double lastThickness, double nextThickness, DraggableStation station, AppTemplate initApp){
        this.lastThickness = lastThickness;
        this.nextThickness = nextThickness;
        this.app = initApp;
        this.station = station;
    }

    @Override
     /**
     * Function does a redo of changing the radius of an object
     */
    public void doTransaction() {
        dataManager = (mapData) app.getDataComponent();
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());
        workspace.getStationThicknessSlider().setValue(nextThickness);
        station.setRadiusX(nextThickness);
        station.setRadiusY(nextThickness);
    }

    @Override
         /**
     * Function does a undo of changing the radius of an object
     */
    public void undoTransaction() {
        dataManager = (mapData) app.getDataComponent();
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());
        workspace.getStationThicknessSlider().setValue(lastThickness);
        station.setRadiusX(lastThickness);
        station.setRadiusY(lastThickness); 
    }
}
