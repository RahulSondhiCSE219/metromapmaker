package map.tps;

import jtps.jTPS_Transaction;
import djf.AppTemplate;
import djf.ui.AppGUI;
import java.util.ArrayList;
import javafx.scene.Node;
import map.data.DraggableLine;
import map.data.DraggableStation;
import map.data.DraggableText;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing adding of objects
 *
 * @author Rahul Sondhi
 */
public class tpsRemoveStat implements jTPS_Transaction {

    DraggableStation allShape;
    DraggableText start;
    AppTemplate app;
    protected AppGUI gui;
    ArrayList<DraggableLine> lines = new ArrayList<>();

    /**
     * Constructor for adding transaction
     *
     * @param initApp
     * @param allShape
     */
    public tpsRemoveStat(AppTemplate initApp, DraggableStation allShape, DraggableText start, ArrayList<DraggableLine> lines) {
        this.allShape = allShape;
        app = initApp;
        this.start = start;
        this.lines = lines;
    }

    @Override
    /**
     * Functions does the redo of adding a shape
     */
    public void doTransaction() {
        for (int i = 0; i < lines.size(); i++) {
            DraggableLine line = lines.get(i);
            if (line.stations.contains(allShape)) {
                line.removeStations(allShape);
            }
        }
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(allShape);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(start);
        ((mapWorkspace) app.getWorkspaceComponent()).destStatName.remove(allShape.name);
        ((mapWorkspace) app.getWorkspaceComponent()).originStatName.remove(allShape.name);
        ((mapWorkspace) app.getWorkspaceComponent()).statNames.remove(allShape.name);

    }

    @Override
    /**
     * Function does the undo of adding a shape
     */
    public void undoTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(allShape);
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(start);
        ((mapWorkspace) app.getWorkspaceComponent()).destStatName.add(allShape.name);
        ((mapWorkspace) app.getWorkspaceComponent()).originStatName.add(allShape.name);
        ((mapWorkspace) app.getWorkspaceComponent()).statNames.add(allShape.name);
        for (int i = 0; i < lines.size(); i++) {
            DraggableLine line = lines.get(i);
            System.out.println(line);
            if (lines.contains(line)) {
                line.addStations(allShape);
                System.out.println(line);
            }
        }
    }

}
