package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import map.data.mapData;
import jtps.jTPS_Transaction;

public class MoveBackToFront implements jTPS_Transaction{
    
    AppTemplate app;
    mapData dataManager;
    protected AppGUI gui;
    
    public MoveBackToFront(AppTemplate initApp){
        this.app = initApp;
    }
    

    @Override
    public void doTransaction() {
        dataManager = (mapData)app.getDataComponent();
        dataManager.moveSelectedShapeToFront();
        app.getGUI().updateToolbarControls(false);
    }

    @Override
    public void undoTransaction() {
        dataManager = (mapData)app.getDataComponent();
        dataManager.moveSelectedShapeToBack();
        app.getGUI().updateToolbarControls(false);
    }
    
}
