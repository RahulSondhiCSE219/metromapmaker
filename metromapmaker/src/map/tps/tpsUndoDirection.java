/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import jtps.jTPS_Transaction;

/**
 * This class provides a transaction for undoing/redoing
 * direction setting
 * @author Rahul Sondhi
 */
public class tpsUndoDirection implements jTPS_Transaction{
    
    /**
     * Constructor for direction transaction
     */
    public tpsUndoDirection(){

    }
    
    @Override
    /**
     * Function does a redo of direction setting
     */
    public void doTransaction() {
        
    }
    
    @Override
    /**
     * Function does a undo of direction setting
     */
    public void undoTransaction() {
        
    }
    
}
