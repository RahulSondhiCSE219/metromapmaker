/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map.tps;

import djf.AppTemplate;
import djf.ui.AppGUI;
import javafx.scene.Node;
import jtps.jTPS_Transaction;
import map.data.DraggableImage;
import map.gui.mapWorkspace;

/**
 * This class provides a transaction for undoing/redoing
 * adding of backgroundImage
 *
 * @author Rahul Sondhi
 */
public class tpsBackgroundImage implements jTPS_Transaction{
        Node allShape;
        Node prevShape;
    AppTemplate app;
    protected AppGUI gui;

    /**
     * Constructor for adding transaction
     * @param initApp
     * @param allShape 
     */
    public tpsBackgroundImage(AppTemplate initApp, Node allShape, Node prevShape) {
        this.allShape = allShape;
        this.prevShape = prevShape;
        app =initApp;     
    }
    
    @Override
     /**
     * Functions does the redo of adding a background image
     */
    public void doTransaction() {
        mapWorkspace workspace = ((mapWorkspace) app.getWorkspaceComponent());    
        
        for(int i = 0; i < workspace.getCanvas().getChildren().size(); i ++){
                if(workspace.getCanvas().getChildren().get(i) instanceof DraggableImage){
                    if(((DraggableImage)workspace.getCanvas().getChildren().get(i)).background == true){
                        workspace.getCanvas().getChildren().remove(i);
                    }
                }
            }
        workspace.getCanvas().getChildren().add(allShape);
    }

    @Override
     /**
     * Function does the undo of adding a background image
     */
        public void undoTransaction() {
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().remove(allShape);
        if(prevShape != null){
            ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(prevShape);
        }
    }
}
