package map.file;

import djf.AppTemplate;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import map.data.mapData;
import map.data.DraggableEllipse;
import map.data.DraggableRectangle;
import map.data.Draggable;
import static map.data.Draggable.ELLIPSE;
import static map.data.Draggable.IMAGE;
import static map.data.Draggable.RECTANGLE;
import static map.data.Draggable.TEXT;
import map.data.DraggableImage;
import map.data.DraggableText;
import static map.data.mapData.WHITE_HEX;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javax.imageio.ImageIO;
import javax.json.JsonString;
import map.data.DraggableLine;
import map.data.DraggableStation;
import map.gui.mapWorkspace;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author Rahul Sondhi
 * @version 1.0
 */
public class mapFiles implements AppFileComponent {

    // FOR JSON LOADING
    static final String JSON_BG_COLOR = "background_color";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";
    static final String JSON_SHAPES = "shapes";
    static final String JSON_SHAPE = "shape";
    static final String JSON_TYPE = "type";
    static final String JSON_NAME = "name";
    static final String JSON_CIRCULAR = "circular";
    static final String JSON_STATIONNAMES = "station_names";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_WIDTH = "width";
    static final String JSON_HEIGHT = "height";
    static final String JSON_FILL_COLOR = "fill_color";
    static final String JSON_OUTLINE_COLOR = "outline_color";
    static final String JSON_OUTLINE_THICKNESS = "outline_thickness";
    static final String JSON_TEXT = "text";
    static final String JSON_FONT_FAMILY = "font_family";
    static final String JSON_FONT_SIZE = "font_size";
    static final String JSON_BOLD = "bold";
    static final String JSON_ITALIC = "italic";
    static final String JSON_IMAGE_PATH = "image_path";
    static final String JSON_STARTX = "startX";
    static final String JSON_STARTY = "startY";
    static final String JSON_ENDX = "endX";
    static final String JSON_ENDY = "endY";
    static final String JSON_ROTATE = "rotate";
    static final String JSON_LABELSTATE = "labelState";
    static final String JSON_BACKBOOL = "backgroundBool";
    static final String JSON_LINETITLE = "lines";
    static final String JSON_STATIONTITLE = "stations";
    static final String JSON_IMAGETITLE = "images";
    static final String JSON_LABELTITLE = "labels";
    

    static final String DEFAULT_DOCTYPE_DECLARATION = "<!doctype html>\n";
    static final String DEFAULT_ATTRIBUTE_VALUE = "";

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that together draws the logo.
     *
     * @param data The data management component for this application.
     *
     * @param filePath Path (including file name/extension) to where to save the
     * data to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        mapData dataManager = (mapData) data;

        // FIRST THE BACKGROUND COLOR
        Color bgColor = dataManager.getBackgroundColor();
        JsonObject bgColorJson = makeJsonColorObject(bgColor);

        // NOW BUILD THE JSON OBJCTS TO SAVE
        JsonArrayBuilder lineBuilder = Json.createArrayBuilder();
        JsonArrayBuilder stationBuilder = Json.createArrayBuilder();
        JsonArrayBuilder labelBuilder = Json.createArrayBuilder();
        JsonArrayBuilder imageBuilder = Json.createArrayBuilder();
        
        ObservableList<Node> shapes = dataManager.getShapes();
        for (Node node : shapes) {
            if (node instanceof DraggableLine) {
                Shape shape = (Shape) node;
                DraggableLine draggableShape = ((DraggableLine) shape);
                String name = draggableShape.name;
                
                JsonObject outlineColorJson = makeJsonColorObject((Color) shape.getStroke());
                double outlineThickness = shape.getStrokeWidth();
                boolean circle = draggableShape.circular;
                double startx = ((MoveTo)draggableShape.getElements().get(0)).getX();
                double starty = ((MoveTo)draggableShape.getElements().get(0)).getY();
                double endx = ((LineTo)draggableShape.getElements().get(draggableShape.getElements().size()-1)).getX();
                double endy = ((LineTo)draggableShape.getElements().get(draggableShape.getElements().size()-1)).getY();
                JsonArrayBuilder stationsinlineBuilder = Json.createArrayBuilder();
                
                for(int i =0; i<draggableShape.stations.size();i++){
                  stationsinlineBuilder.add(((DraggableStation)draggableShape.stations.get(i)).name);
                }
                
                JsonArray stationinlineArray = stationsinlineBuilder.build();
                
                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_CIRCULAR, circle)
                        .add(JSON_OUTLINE_COLOR, outlineColorJson)
                        .add(JSON_STATIONNAMES,stationinlineArray)
                        .add(JSON_OUTLINE_THICKNESS, outlineThickness)
                        .add(JSON_STARTX,startx)
                        .add(JSON_STARTY,starty)
                        .add(JSON_ENDX,endx)
                        .add(JSON_ENDY,endy)
                        .build();
                lineBuilder.add(shapeJson);
            

            } else if (node instanceof DraggableText) {
                //DraggableText shape = (DraggableText) node;
                Shape shape = (Shape) node;
                DraggableText draggableShape = ((DraggableText) shape);
                if(draggableShape.isLabel){
                String type = draggableShape.getShapeType();
                double x = draggableShape.getX();
                double y = draggableShape.getY();
                boolean bold = ((DraggableText) draggableShape).getBold();
                boolean italic = ((DraggableText) draggableShape).getItalics();
                String fontFamily = ((DraggableText) draggableShape).getFont().getFamily();
                double fontSize = ((DraggableText) draggableShape).getFont().getSize();
                String text = ((DraggableText) draggableShape).getText();
                JsonObject fillColorJson = makeJsonColorObject((Color) shape.getFill());

                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_TEXT, text)
                        .add(JSON_FILL_COLOR, fillColorJson)
                        .add(JSON_BOLD, bold)
                        .add(JSON_ITALIC, italic)
                        .add(JSON_FONT_SIZE, fontSize)
                        .add(JSON_FONT_FAMILY, fontFamily).build();
                labelBuilder.add(shapeJson);
                }
            }
            else if (node instanceof DraggableStation) {
                Shape shape = (Shape) node;
                DraggableStation draggableShape = ((DraggableStation) shape);
                String name = draggableShape.name;
                double x = draggableShape.getX();
                double y = draggableShape.getY();
                double width = draggableShape.getWidth();
                double height = draggableShape.getHeight();
                double rotate = draggableShape.rotate;
                double labelState = draggableShape.labelState;
                JsonObject fillColorJson = makeJsonColorObject((Color) shape.getFill());

                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_WIDTH, width)
                        .add(JSON_HEIGHT, height)
                        .add(JSON_FILL_COLOR, fillColorJson)
                        .add(JSON_ROTATE, rotate)
                        .add(JSON_LABELSTATE, labelState)
                        .build();
                stationBuilder.add(shapeJson);
                
            }  else if (node instanceof DraggableImage) {
                DraggableImage shape = (DraggableImage) node;
                Draggable draggableShape = ((Draggable) shape);
                String type = draggableShape.getShapeType();
                String path = ((DraggableImage) draggableShape).path.substring(5);
                double x = draggableShape.getX();
                double y = draggableShape.getY();
                double width = draggableShape.getWidth();
                double height = draggableShape.getHeight();
                boolean background = shape.background;

                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_WIDTH, width)
                        .add(JSON_HEIGHT, height)
                        .add(JSON_IMAGE_PATH, path)
                        .add(JSON_BACKBOOL, background)
                        .build();

                imageBuilder.add(shapeJson);

            }
        }
        JsonArray linesArray = lineBuilder.build();
        JsonArray stationsArray = stationBuilder.build();
        JsonArray labelArray = labelBuilder.build();
        JsonArray imageArray = imageBuilder.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_BG_COLOR, bgColorJson)
                .add(JSON_LINETITLE, linesArray)
                .add(JSON_STATIONTITLE, stationsArray)
                .add(JSON_LABELTITLE, labelArray)
                .add(JSON_IMAGETITLE, imageArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }
    
    
    
    /**
     * Function makesJsonColorObject for when loading color back in
     * @param color
     * @return JsonObject
     */
    private JsonObject makeJsonColorObject(Color color) {
        JsonObject colorJson = Json.createObjectBuilder()
                .add(JSON_RED, color.getRed())
                .add(JSON_GREEN, color.getGreen())
                .add(JSON_BLUE, color.getBlue())
                .add(JSON_ALPHA, color.getOpacity()).build();
        return colorJson;
    }

    /**
     * This method loads data from a JSON formatted file into the data
     * management component and then forces the updating of the workspace such
     * that the user may edit the data.
     *
     * @param data Data management component where we'll load the file into.
     *
     * @param filePath Path (including file name/extension) to where to load the
     * data from.
     *
     * @throws IOException Thrown should there be an error reading in data from
     * the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
        mapData dataManager = (mapData) data;
        dataManager.resetData();

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        // LOAD THE BACKGROUND COLOR
        Color bgColor = loadColor(json, JSON_BG_COLOR);
        dataManager.setBackgroundColor(bgColor);
        
        try{
        // AND NOW LOAD ALL THE SHAPES
        JsonArray jsonShapeArray = json.getJsonArray(JSON_STATIONTITLE);
        for (int i = 0; i < jsonShapeArray.size(); i++) {
            JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
            
            dataManager.startNewStation(
                    (int)getDataAsDouble(jsonShape, JSON_X), 
                    (int)getDataAsDouble(jsonShape, JSON_Y),
                    getDataAsString(jsonShape, JSON_NAME),
                    (int)getDataAsDouble(jsonShape, JSON_WIDTH), 
                    (int)getDataAsDouble(jsonShape, JSON_HEIGHT),
                    loadColor(jsonShape, JSON_FILL_COLOR),
                    getDataAsDouble(jsonShape, JSON_ROTATE),
                    (int)getDataAsDouble(jsonShape, JSON_LABELSTATE)
            );
        }
        
        jsonShapeArray = json.getJsonArray(JSON_LINETITLE);
        for (int i = 0; i < jsonShapeArray.size(); i++) {
            JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
            
            dataManager.startNewLine(
                    getDataAsString(jsonShape, JSON_NAME),
                    getDataAsBoolean(jsonShape,JSON_CIRCULAR),
                    loadColor(jsonShape, JSON_OUTLINE_COLOR),
                    getDataAsStations(jsonShape),
                    (int)getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS),
                    (int)getDataAsDouble(jsonShape, JSON_STARTX), 
                    (int)getDataAsDouble(jsonShape, JSON_STARTY),
                    (int)getDataAsDouble(jsonShape, JSON_ENDX), 
                    (int)getDataAsDouble(jsonShape, JSON_ENDY)
            );
        }
//               
        jsonShapeArray = json.getJsonArray(JSON_IMAGETITLE);
        for (int i = 0; i < jsonShapeArray.size(); i++) {
            JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
            
            dataManager.startNewImage(
                    (int)getDataAsDouble(jsonShape, JSON_X), 
                    (int)getDataAsDouble(jsonShape, JSON_Y),
                    (int)getDataAsDouble(jsonShape, JSON_WIDTH), 
                    (int)getDataAsDouble(jsonShape, JSON_HEIGHT),
                    getDataAsString(jsonShape, JSON_IMAGE_PATH),
                    getDataAsBoolean(jsonShape,JSON_BACKBOOL)
            );
        }
        
        jsonShapeArray = json.getJsonArray(JSON_LABELTITLE);
        for (int i = 0; i < jsonShapeArray.size(); i++) {
            JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
            
            dataManager.startNewText(
                    (int)getDataAsDouble(jsonShape, JSON_X), 
                    (int)getDataAsDouble(jsonShape, JSON_Y),
                    getDataAsString(jsonShape, JSON_TEXT),
                    loadColor(jsonShape, JSON_FILL_COLOR),
                    (int)getDataAsDouble(jsonShape, JSON_FONT_SIZE),
                    getDataAsBoolean(jsonShape,JSON_BOLD),
                    getDataAsBoolean(jsonShape,JSON_ITALIC),
                    getDataAsString(jsonShape, JSON_FONT_FAMILY)
            );
        }
        }catch(NullPointerException e){
           dataManager.resetData(); 
        }
        
    }
    
    /**
     * This function takes JSON object and turns into a double
     * @param json
     * @param dataName
     * @return 
     */
    private double getDataAsDouble(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigDecimalValue().doubleValue();
    }
    
    private ArrayList<String> getDataAsStations(JsonObject json) {
        ArrayList<String> send = new ArrayList<>();
        JsonArray jsonShapeArray = json.getJsonArray(JSON_STATIONNAMES);
        for (int i = 0; i < jsonShapeArray.size(); i++) {
            String jsonShape = jsonShapeArray.getJsonString(i).toString();
            send.add(jsonShape);
        }
        return send;
    }
    /**
     * This function takes JSON object and turns into a String
     * @param json
     * @param dataName
     * @return String
     */
    private String getDataAsString(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonString number = (JsonString) value;
        return number.getString();
    }
    
    /**
     * This function takes JSON object and turns into a Boolean
     * @param json
     * @param dataName
     * @return boolean
     */
    private boolean getDataAsBoolean(JsonObject json, String dataName) {
        //JsonValue value = json.get(dataName);
        //JsonNumber number = (JsonNumber) value;
        return json.getBoolean(dataName);
    }

    /**
     * Helper function to load shapes
     * @param jsonShape
     * @return 
     */
    private Shape loadShape(JsonObject jsonShape) {
        // FIRST BUILD THE PROPER SHAPE TYPE
        String type = jsonShape.getString(JSON_TYPE);
        Shape shape = null;
        switch (type) {
            case RECTANGLE:
                shape = new DraggableRectangle();
                break;
            case ELLIPSE:
                shape = new DraggableEllipse();
                break;
            case TEXT:
                String text1 = getDataAsString(jsonShape, JSON_TEXT);
                double x1 = getDataAsDouble(jsonShape, JSON_X);
                double y1 = getDataAsDouble(jsonShape, JSON_Y);
                shape = new DraggableText(text1,x1,y1);
                break;
            case IMAGE:
                String imagePath1 = getDataAsString(jsonShape, JSON_IMAGE_PATH);
                shape = new DraggableImage(imagePath1);
                break;
            default:
                break;
        }

        // THEN LOAD ITS FILL AND OUTLINE PROPERTIES
        if (shape instanceof DraggableRectangle || shape instanceof DraggableEllipse) {
            Color fillColor = loadColor(jsonShape, JSON_FILL_COLOR);
            Color outlineColor = loadColor(jsonShape, JSON_OUTLINE_COLOR);
            double outlineThickness = getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS);
            shape.setFill(fillColor);
            shape.setStroke(outlineColor);
            shape.setStrokeWidth(outlineThickness);

            // AND THEN ITS DRAGGABLE PROPERTIES
            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            double width = getDataAsDouble(jsonShape, JSON_WIDTH);
            double height = getDataAsDouble(jsonShape, JSON_HEIGHT);
            Draggable draggableShape = (Draggable) shape;
            draggableShape.setLocationAndSize(x, y, width, height);
            
            // ALL DONE, RETURN IT
            
        } 
        else if (type.equals(TEXT)) {
            Color fillColor = loadColor(jsonShape, JSON_FILL_COLOR);
            Color outlineColor = loadColor(jsonShape, JSON_OUTLINE_COLOR);
            String text = getDataAsString(jsonShape, JSON_TEXT);
            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            double outlineThickness = getDataAsDouble(jsonShape,JSON_OUTLINE_THICKNESS);
            //double height = getDataAsDouble(jsonShape, JSON_HEIGHT);
            Draggable draggableShape = (Draggable) shape;
            String fontFamily = getDataAsString(jsonShape, JSON_FONT_FAMILY);
            double fontSize = getDataAsDouble(jsonShape, JSON_FONT_SIZE);
            boolean bold = getDataAsBoolean(jsonShape,JSON_BOLD);
            boolean italic = getDataAsBoolean(jsonShape,JSON_ITALIC);
            shape.setFill(fillColor);
            ((DraggableText)shape).setBold(bold);
            ((DraggableText)shape).setItalics(italic);
            
            if  (bold && italic)
                ((DraggableText)shape).setFont(Font.font(fontFamily,FontWeight.BOLD,FontPosture.ITALIC, fontSize));
            else if (bold)
            ((DraggableText)shape).setFont(Font.font(fontFamily,FontWeight.BOLD, fontSize));
            else if (italic)
                ((DraggableText)shape).setFont(Font.font(fontFamily,FontPosture.ITALIC, fontSize));
            else if (bold && italic)
                ((DraggableText)shape).setFont(Font.font(fontFamily,FontWeight.BOLD,FontPosture.ITALIC, fontSize));
            shape.setStroke(outlineColor);
            shape.setStrokeWidth(outlineThickness);
            

        }
        else if (type.equals(IMAGE)) {
                double x = getDataAsDouble(jsonShape, JSON_X);
                double y = getDataAsDouble(jsonShape, JSON_Y);
                Draggable draggableShape = (Draggable) shape;
                draggableShape.start((int)x,(int)y);
        } 
        
        
        return shape;
    }
    
    /**
     * Helper function to load in color
     * @param json
     * @param colorToGet
     * @return 
     */
    private Color loadColor(JsonObject json, String colorToGet) {
        JsonObject jsonColor = json.getJsonObject(colorToGet);
        double red = getDataAsDouble(jsonColor, JSON_RED);
        double green = getDataAsDouble(jsonColor, JSON_GREEN);
        double blue = getDataAsDouble(jsonColor, JSON_BLUE);
        double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
        Color loadedColor = new Color(red, green, blue, alpha);
        return loadedColor;
    }

    /**
     * HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
     * @param jsonFilePath
     * @return
     * @throws IOException 
     */
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }


    @Override
    /**
     * This method is provided to satisfy the compiler, but it is not used by
     * this application.
     */
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        mapData dataManager = (mapData) data;      
        String MapName = dataManager.getMap();
        // NOW BUILD THE JSON OBJCTS TO SAVE
        JsonArrayBuilder lineBuilder = Json.createArrayBuilder();
        JsonArrayBuilder stationBuilder = Json.createArrayBuilder();
        
        ObservableList<Node> shapes = dataManager.getShapes();
        for (Node node : shapes) {
            if (node instanceof DraggableStation) {
                Shape shape = (Shape) node;
                DraggableStation draggableShape = ((DraggableStation) shape);
                String name = draggableShape.name;
                double x = draggableShape.getX();
                double y = draggableShape.getY();

                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .build();
                stationBuilder.add(shapeJson);
            }
            else if (node instanceof DraggableLine) {
                Shape shape = (Shape) node;
                DraggableLine draggableShape = ((DraggableLine) shape);
                String name = draggableShape.name;
                
                JsonObject outlineColorJson = makeJsonColorObject((Color) shape.getStroke());
                boolean circle = draggableShape.circular;
                JsonArrayBuilder stationsinlineBuilder = Json.createArrayBuilder();
                
                for(int i =0; i<draggableShape.stations.size();i++){
                  stationsinlineBuilder.add(((DraggableStation)draggableShape.stations.get(i)).name);
                }
                
                JsonArray stationinlineArray = stationsinlineBuilder.build();
                
                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_CIRCULAR, circle)
                        .add(JSON_OUTLINE_COLOR, outlineColorJson)
                        .add(JSON_STATIONNAMES,stationinlineArray)
                        .build();
                lineBuilder.add(shapeJson);
            }
        }
        JsonArray linesArray = lineBuilder.build();
        JsonArray stationsArray = stationBuilder.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_NAME, MapName)
                .add(JSON_LINETITLE, linesArray)
                .add(JSON_STATIONTITLE, stationsArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        File newFile = new File(filePath.substring(0, filePath.lastIndexOf("/")) + "/Export/" +filePath.substring(filePath.lastIndexOf("/"),filePath.length()));
        OutputStream os = new FileOutputStream(newFile.getAbsolutePath());
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(newFile.getAbsolutePath());
        pw.write(prettyPrinted);
        pw.close();
        dataManager.processSnapshot(filePath.substring(0, filePath.lastIndexOf("/")) + "/Export/" +filePath.substring(filePath.lastIndexOf("/"),filePath.lastIndexOf("."))+".png");
    }

    /**
     * This method is provided to satisfy the compiler, but it is not used by
     * this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        // AGAIN, WE ARE NOT USING THIS IN THIS ASSIGNMENT
    }

}