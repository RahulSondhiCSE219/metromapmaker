package map.data;

import djf.AppTemplate;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import map.tps.tpsDrag;
import java.io.File;
import java.net.MalformedURLException;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import jtps.jTPS;
import jtps.jTPS_Transaction;

/**
 * This is a draggable rectangle for our goLogoLo application.
 * FOR HW2
 * @author Richard McKenna
 * @author Rahul Sondhi
 * @version 1.0
 */
public class DraggableImage extends Rectangle implements Draggable {
    public int startX;
    public int startY;
    public String path;
    public Image image;
    public boolean background;
    jTPS transact;
    jTPS_Transaction t;
    
    public DraggableImage(String path) {
        background = false;
        this.path = path;
        this.image = new Image(path);
        setFill(new ImagePattern(image));
	setX(0);
	setY(0);
	setWidth(image.getWidth());
	setHeight(image.getHeight());
	setOpacity(1.0);
	startX = 0;
	startY = 0;
    }
    
    @Override
    public mapState getStartingState() {
	return mapState.STARTING_IMAGE;
    }
    
       @Override
    public void start(int x, int y) {
	setX(x);
	setY(y);
    }
    
    @Override
    public double getStartX(){
        return startX;
    }
    
    @Override
    public double getStartY(){
        return startY;
    }
    
    public void startXY(int x, int y) {
	startX = x;
	startY = y;
    }
    
    @Override
    public void drag(int x, int y) {
      if(!background){  
       double diffX = x - startX;
	double diffY = y - startY;
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	setX(newX);
	setY(newY);
	startX = x;
	startY = y;
      }
    }
    
    public String cT(double x, double y) {
	return "(x,y): (" + x + "," + y + ")";
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - getX();
	widthProperty().set(width);
	double height = y - getY();
	heightProperty().set(height);	
    }
    
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	xProperty().set(initX);
	yProperty().set(initY);
	widthProperty().set(initWidth);
	heightProperty().set(initHeight);
    }
    
    @Override
    public String getShapeType() {
	return IMAGE;
    }
    
    public void setImage(){
        FileChooser fc = new FileChooser();

        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        fc.setInitialDirectory(new File(PATH_IMAGES));
        fc.setTitle("Image:");

        File selectedFile = fc.showOpenDialog(null);
        if (selectedFile != null) {
                Image fuck = new Image(selectedFile.toURI().toString());
                setFill(new ImagePattern(fuck));
        }
    }
    
    public Image getImage(){
        return image;
    }
}
