package map.data;

/**
 * This enum has the various possible states of the logo maker app
 * during the editing process which helps us determine which controls
 * are usable or not and what specific user actions should affect.
 * 
 * @author Richard McKenna
 * @author Rahul Sondhi
 * @version 1.0
 */
public enum mapState {
    SELECTING_SHAPE,
    DRAGGING_SHAPE,
    STARTING_RECTANGLE,
    STARTING_ELLIPSE,
    SIZING_SHAPE,
    DRAGGING_NOTHING,
    SIZING_NOTHING, 
    
    //*************************
    STARTING_TEXT, 
    STARTING_IMAGE,
    STARTING_LINE, 
    STARTING_STATION,
    REMOVING_STATION,
    REMOVING_LINE, 
    REMOVING_STATLINE, 
    ADDING_STATLINE, DRAGGING_LABEL, DRAGGING_LABELDROP
}
