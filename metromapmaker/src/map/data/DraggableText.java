package map.data;

import djf.AppTemplate;
import map.tps.tpsDrag;
import java.util.NoSuchElementException;
import java.util.Optional;
import javafx.scene.control.TextInputDialog;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import jtps.jTPS;
import jtps.jTPS_Transaction;

/**
 * This is a draggable text for our metro map application.
 * @author Rahul Sondhi
 */
public class DraggableText extends Text implements Draggable {
    public double startX;
    public double startY;
    boolean bold;
    boolean italic;
    public boolean inLine;
    public boolean isLabel;
    public boolean inStation;
    DraggableEllipse tempShape = new DraggableEllipse();
    String text;
    jTPS transact;
    jTPS_Transaction t;
    
    /**
     * Constructor for text
     * @param data
     * @param x
     * @param y 
     */
    public DraggableText(String data, double x, double y) {
        setText(data);
	setOpacity(1.0);
        setX(x);
	setY(y);
	startX = 0;
	startY = 0;
        inLine = false;
        inStation = false;
        bold = false;
        italic = false;
        isLabel = true;
        tempShape.size(20, 20);
        tempShape.setOpacity(0.0);
        tempShape.setFill(Paint.valueOf("red"));
        tempShape.setStroke(Paint.valueOf("black"));
    }
    
    @Override
    /**
     * Gets object state
     */
    public mapState getStartingState() {
	return mapState.STARTING_TEXT;
    }
    
    @Override
    /**
     * Sets x and y
     */
    public void start(int x, int y) {
	setX(x);
	setY(y);
    }
    
    /**
     * Sets startx and starty
     * @param x
     * @param y 
     */
    public void startXY(int x, int y) {
	startX = x;
	startY = y;
    }
    
    @Override
    /**
     * Function for drag
     */
    public void drag(int x, int y) {
        double diffX = x - startX;
	double diffY = y - startY;
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	setX(newX);
	setY(newY);
	startX = x;
	startY = y;
    }
    
    @Override
    public void size(int x, int y) {
	
    }

    @Override
    public double getWidth() {
	return this.getWidth();
    }

    @Override
    public double getHeight() {
	return this.getHeight();
    }
        
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	setX(initX + (initWidth/2));
	setY(initY + (initHeight/2));
    }
    
    @Override
    public String getShapeType() {
	return TEXT;
    }
    
    
    public boolean getBold(){
        return bold;
    }
    
    public boolean getItalics(){
        return italic;
    }
    
    public void setBold(boolean bold){
        this.bold = bold;
    }
    
    public void setItalics(boolean italic){
     this.italic = italic;
    }
    
    public double getStartX() {
        return startX;
    }

    public void setStartX(double startX) {
        this.startX = startX;
    }

    public double getStartY() {
        return startY;
    }

    public void setStartY(double startY) {
        this.startY = startY;
    }
    
    public void setText(){
        TextInputDialog text = new TextInputDialog("Text");
        text.setTitle("Text Input");
        text.setContentText("Text to add:");
        Optional<String> data = text.showAndWait();
        try{
        setText(data.get());
        }catch(NoSuchElementException e){
            
        }
    }

    public void endDrag() {
       if(inLine){
        setOpacity(1.0);
        tempShape.setOpacity(0.0);
       }else if(inStation){
        isLabel = false;
       }
    }

    public void startDrag() {
        setOpacity(0.0);
        tempShape.start((int)getX(), (int)getY());
        tempShape.centerXProperty().bindBidirectional(xProperty());
        tempShape.centerYProperty().bindBidirectional(yProperty());
        tempShape.setOpacity(1.0);
    }
}
