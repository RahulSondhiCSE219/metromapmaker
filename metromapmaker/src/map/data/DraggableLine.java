package map.data;

import djf.AppTemplate;
import java.util.ArrayList;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;
import javafx.scene.shape.Shape;
import map.tps.tpsDrag;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import map.gui.mapWorkspace;

/**
 * This is draggable object for the lines.
 *
 * @author Rahul Sondhi
 */
public class DraggableLine extends Path implements Draggable {

    public double startX;
    public double startY;
    public String name;
    public DraggableText startText;
    public DraggableText endText;
    public boolean circular;
    jTPS transact;
    jTPS_Transaction t;
    public ArrayList<DraggableStation> stations = new ArrayList<>();

    public DraggableLine(String name) {
        setOpacity(1.0);
        setFill(Color.TRANSPARENT);
        startX = 0.0;
        startY = 0.0;
        this.name = name;
        setStrokeWidth(3);
        circular = false;
    }

    @Override
    public mapState getStartingState() {
        return mapState.STARTING_LINE;
    }

    @Override
    public void start(int x, int y) {
        MoveTo start = new MoveTo(x, y);
        LineTo end = new LineTo(x + 500, y);

        getElements().add(start);
        getElements().add(end);

        startText = new DraggableText(name, x, y);
        endText = new DraggableText(name, x + 500, y);
        startText.inLine = true;
        startText.isLabel = false;
        endText.inLine = true;
        endText.isLabel = false;
        start.xProperty().bindBidirectional(startText.xProperty());
        start.yProperty().bindBidirectional(startText.yProperty());
        end.xProperty().bindBidirectional(endText.xProperty());
        end.yProperty().bindBidirectional(endText.yProperty());

    }

    public void start(int x, int y, int x2, int y2) {
        MoveTo start = new MoveTo(x, y);
        LineTo end = new LineTo(x2, y2);

        getElements().add(start);
        getElements().add(end);

        startText = new DraggableText(name, x, y);
        endText = new DraggableText(name, x2, y2);
        startText.inLine = true;
        startText.isLabel = false;
        endText.inLine = true;
        endText.isLabel = false;
        start.xProperty().bindBidirectional(startText.xProperty());
        start.yProperty().bindBidirectional(startText.yProperty());
        end.xProperty().bindBidirectional(endText.xProperty());
        end.yProperty().bindBidirectional(endText.yProperty());

    }

    public void addStations(DraggableStation station) {
        stations.add(station);
        LineTo addedLine = new LineTo(station.getCenterX(), station.getCenterY());
        getElements().add(distanceCheck(addedLine), addedLine);
        addedLine.xProperty().bind(station.centerXProperty());
        addedLine.yProperty().bind(station.centerYProperty());
    }

    private int distanceCheck(LineTo addedLine) {

        Point2D p1 = new Point2D(addedLine.getX(), addedLine.getY());
        Point2D p2 = new Point2D(((MoveTo) getElements().get(0)).getX(), ((MoveTo) getElements().get(0)).getY());
        double minDist = p1.distance(p2);
        int indexOfMin = 0;
        for (PathElement p : getElements().subList(1, getElements().size() - 1)) {
            p2 = new Point2D(((LineTo) p).getX(), ((LineTo) p).getY());
            if (p1.distance(p2) < minDist) {
                minDist = p1.distance(p2);
                indexOfMin = getElements().indexOf(p);

            }
        }
        if (indexOfMin == 0) {
            return 1;

        } else if (indexOfMin == getElements().size() - 1) {
            return getElements().size() - 2;
        } else {
            Point2D minOne;
            Point2D minTwo;
            if (getElements().get(indexOfMin-1) instanceof MoveTo) {
                minOne = new Point2D(((MoveTo) getElements().get(indexOfMin - 1)).getX(), ((MoveTo) getElements().get(indexOfMin - 1)).getY());
            }else{
                minOne = new Point2D(((LineTo) getElements().get(indexOfMin - 1)).getX(), ((LineTo) getElements().get(indexOfMin - 1)).getY());
            }
            
            if (getElements().get(indexOfMin+1) instanceof MoveTo) {
                minTwo = new Point2D(((MoveTo) getElements().get(indexOfMin + 1)).getX(), ((MoveTo) getElements().get(indexOfMin + 1)).getY());
            }else{
                minTwo = new Point2D(((LineTo) getElements().get(indexOfMin + 1)).getX(), ((LineTo) getElements().get(indexOfMin + 1)).getY());
            }
            
            if (minOne.distance(p2) >= minTwo.distance(p2)) {
                return indexOfMin;
            } else {
                return indexOfMin + 1;
            }
        }
    }

    public void removeStations(DraggableStation station) {
        if (stations.contains(station)) {
            stations.remove(station);
            for (int i = 0; i < getElements().size(); i++) {
                if (getElements().get(i) instanceof MoveTo) {
                    MoveTo line = (MoveTo) getElements().get(i);

                    if ((station.getCenterX() == line.getX()) && (station.getCenterY() == line.getY())) {
                        getElements().remove(i);
                        break;
                    }
                } else {
                    LineTo line = (LineTo) getElements().get(i);
                    if ((station.getCenterX() == line.getX()) && (station.getCenterY() == line.getY())) {
                        getElements().remove(i);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void startXY(int x, int y) {
        startX = x;
        startY = y;
    }

    @Override
    public void drag(int x, int y) {

    }

    public String cT(double x, double y) {
        return "(x,y): (" + x + "," + y + ")";
    }

    @Override
    public void size(int x, int y) {

    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
//	xProperty().set(initX+(initWidth/2));
//	yProperty().set(initY+(heightProperty().getValue()/2));
    }

    @Override
    public String getShapeType() {
        return LINE;
    }

    @Override
    public double getX() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getY() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getWidth() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getHeight() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getStartX() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getStartY() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
