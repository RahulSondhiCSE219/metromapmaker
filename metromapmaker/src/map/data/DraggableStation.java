package map.data;

import djf.AppTemplate;
import javafx.scene.shape.Ellipse;
import map.tps.tpsDrag;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import map.data.mapData;

/**
 * This is draggable object for stations.
 * @author Rahul Sondhi
 */
public class DraggableStation extends Ellipse implements Draggable {
    public double startCenterX;
    public double startCenterY;
    public int labelState;
    public double rotate;
    public String name;
    public DraggableText labelText;
    jTPS transact;
    jTPS_Transaction t;
    
    public DraggableStation(String name) {
        setCenterX(0.0);
	setCenterY(0.0);
	setRadiusX(10.0);
	setRadiusY(10.0);
	setOpacity(1.0);
	setOpacity(1.0);
	startCenterX = 0.0;
	startCenterY = 0.0;
        this.name=name;
        labelState = 1;
        rotate = 0.0;
    }
    
    @Override
    public mapState getStartingState() {
	return mapState.STARTING_STATION;
    }
    
    @Override
    public double getStartX(){
        return startCenterX;
    }
    
    @Override
    public double getStartY(){
        return startCenterY;
    }
    
    @Override
    public void start(int x, int y) {
	setCenterX(x);
	setCenterY(y);
        labelText= new DraggableText(name, x+20, y+20);
        labelText.inStation = true;
        labelText.isLabel = false;
        labelText.xProperty().bind(centerXProperty().add(20.00));
        labelText.yProperty().bind(centerYProperty().add(20.00));
    }
    
    @Override
    public void startXY(int x, int y) {
	startCenterX = x;
	startCenterY = y;
    }
    
    @Override
    public void drag(int x, int y) {
	double diffX = x - startCenterX;
	double diffY = y - startCenterY;
	double newX = getCenterX() + diffX;
	double newY = getCenterY() + diffY;
	setCenterX(newX);
	setCenterY(newY);
	startCenterX = x;
	startCenterY = y;
    }
    
    public String cT(double x, double y) {
	return "(x,y): (" + x + "," + y + ")";
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - startCenterX;
	double height = y - startCenterY;
	double centerX = startCenterX + (width / 2);
	double centerY = startCenterY + (height / 2);
	setCenterX(centerX);
	setCenterY(centerY);
	setRadiusX(width / 2);
	setRadiusY(height / 2);	
    }
    
    @Override
    public double getX() {
	return getCenterX() - getRadiusX();
    }

    @Override
    public double getY() {
	return getCenterY() - getRadiusY();
    }

    @Override
    public double getWidth() {
	return getRadiusX() * 2;
    }

    @Override
    public double getHeight() {
	return getRadiusY() * 2;
    }
        
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	setCenterX(initX + (initWidth/2));
	setCenterY(initY + (initHeight/2));
	setRadiusX(initWidth/2);
	setRadiusY(initHeight/2);
    }
    
    @Override
    public String getShapeType() {
	return STATION;
    }
}
