package map.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import static map.data.mapState.SELECTING_SHAPE;
import static map.data.mapState.SIZING_SHAPE;
import map.gui.mapWorkspace;
import djf.components.AppDataComponent;
import djf.AppTemplate;
import djf.controller.AppFileController;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import djf.ui.AppGUI;
import map.tps.tpsAdd;
import map.tps.tpsBold;
import map.tps.tpsTextSize;
import map.tps.tpsTextFont;
import map.tps.tpsItalicizer;
import map.tps.tpsRemove;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Optional;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.WritableImage;
import javafx.scene.shape.LineTo;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import map.tps.tpsAddLine;
import map.tps.tpsAddStat;
import map.tps.tpsAddStatInLine;
import map.tps.tpsBackgroundImage;
import map.tps.tpsEditLine;
import map.tps.tpsLineThickness;
import map.tps.tpsMoveLabel;
import map.tps.tpsRemoveLine;
import map.tps.tpsRemoveStat;
import map.tps.tpsRemoveStatInLine;
import map.tps.tpsRotate;
import map.tps.tpsStationRadius;
import map.tps.tpsToggleSnap;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author Rahul Sondhi
 * @version 1.0
 */
public class mapData implements AppDataComponent {
    // FIRST THE THINGS THAT HAVE TO BE SAVED TO FILES

    // THESE ARE THE SHAPES TO DRAW
    ObservableList<Node> shapes;

    // THE BACKGROUND COLOR
    Color backgroundColor;

    // AND NOW THE EDITING DATA
    // THIS IS THE SHAPE CURRENTLY BEING SIZED BUT NOT YET ADDED
    Shape newShape;
    Shape clipboardShape;

    // THIS IS THE SHAPE CURRENTLY SELECTED
    Shape selectedShape;
    DraggableLine selectedLine;
    DraggableStation selectedStation;

    // FOR FILL AND OUTLINE
    Color currentFillColor;
    Color currentOutlineColor;
    double currentBorderWidth;

    // CURRENT STATE OF THE APP
    mapState state;

    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    public static AppTemplate app;

    // USE THIS WHEN THE SHAPE IS SELECTED
    Effect highlightedEffect;

    public static final String WHITE_HEX = "#FFFFFF";
    public static final String BLACK_HEX = "#000000";
    public static final String YELLOW_HEX = "#EEEE00";
    public static final Paint DEFAULT_BACKGROUND_COLOR = Paint.valueOf(WHITE_HEX);
    public static final Paint HIGHLIGHTED_COLOR = Paint.valueOf(YELLOW_HEX);
    public static final int HIGHLIGHTED_STROKE_THICKNESS = 2;
    private static jTPS transact;
    private static jTPS_Transaction backing;
    public AppGUI gui;
    mapData dataManager;

    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public mapData(AppTemplate initApp) {
        // KEEP THE APP FOR LATER
        app = initApp;
        gui = app.getGUI();
        // NO SHAPE STARTS OUT AS SELECTED
        newShape = null;
        selectedShape = null;

        // INIT THE COLORS
        currentFillColor = Color.web(WHITE_HEX);
        currentOutlineColor = Color.web(BLACK_HEX);
        currentBorderWidth = 1;

        // THIS IS FOR THE SELECTED SHAPE
        DropShadow dropShadowEffect = new DropShadow();
        dropShadowEffect.setOffsetX(0.0f);
        dropShadowEffect.setOffsetY(0.0f);
        dropShadowEffect.setSpread(1.0);
        dropShadowEffect.setColor(Color.YELLOW);
        dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
        dropShadowEffect.setRadius(15);

        transact = new jTPS();

    }

    /**
     * Gets transaction for undo/redo
     *
     * @return
     */
    public jTPS getTransact() {
        return transact;
    }

    /**
     * Sets transaction for undo/redo
     *
     * @return
     */
    public void setTransact(jTPS transact) {
        mapData.transact = transact;

    }

    public String getMap() {
        return ((AppFileController) app.getGUI().getFileController()).currentWorkFile.getName().substring(0, ((AppFileController) app.getGUI().getFileController()).currentWorkFile.getName().lastIndexOf("."));
    }

    public void processSnapshot(String path) {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
        File file = new File(path);
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Sets transaction for undo/redo
     *
     * @return
     */
    public void setTransaction(jTPS_Transaction backing) {
        mapData.backing = backing;
    }

    /**
     * Gets Backing
     *
     * @return
     */
    public jTPS_Transaction getBacking() {
        return backing;
    }

    /**
     * Function to return list of shapes on canvas
     *
     * @return
     */
    public ObservableList<Node> getShapes() {
        return shapes;
    }

    /**
     * Gets background color
     *
     * @return
     */
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * Gets fill color of current shape
     *
     * @return
     */
    public Color getCurrentFillColor() {
        return currentFillColor;
    }

    /**
     * Gets outline color of current shape
     *
     * @return
     */
    public Color getCurrentOutlineColor() {
        return currentOutlineColor;
    }

    /**
     * Gets current border width of current shape
     *
     * @return
     */
    public double getCurrentBorderWidth() {
        return currentBorderWidth;
    }

    /**
     * Sets list of shapes in canvas
     *
     * @param initShapes
     * @return
     */
    public void setShapes(ObservableList<Node> initShapes) {
        shapes = initShapes;
    }

    /**
     * Sets background color
     *
     * @param initBackgroundColor
     */
    public void setBackgroundColor(Color initBackgroundColor) {
        backgroundColor = initBackgroundColor;
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        BackgroundFill fill = new BackgroundFill(backgroundColor, null, null);
        Background background = new Background(fill);
        canvas.setBackground(background);
    }

    /**
     * Sets fill color of current shape
     *
     * @param initColor
     */
    public void setCurrentFillColor(Color initColor) {
        currentFillColor = initColor;
        if (selectedShape != null && !(selectedShape instanceof DraggableImage)) {
            selectedShape.setFill(currentFillColor);
        }
    }

    /**
     * Sets outline color of current shape
     *
     * @param initColor
     */
    public void setCurrentOutlineColor(Color initColor) {
        currentOutlineColor = initColor;
        if (selectedShape != null) {
            selectedShape.setStroke(initColor);
        }
    }

    /**
     * Sets outline thickness of current shape
     *
     * @param initBorderWidth
     */
    public void setCurrentOutlineThickness(int initBorderWidth) {
        currentBorderWidth = initBorderWidth;
        if (selectedShape != null) {
            selectedShape.setStrokeWidth(initBorderWidth);
        }
    }

    /**
     * Removes selected shape
     */
    public void removeSelectedShape() {
        if (selectedShape != null) {
            if (selectedShape instanceof DraggableLine) {
                System.out.println("Cannot Delete This");
            } else if (selectedShape instanceof DraggableStation) {
                System.out.println("Cannot Delete This");
            } else if (selectedShape instanceof DraggableText) {
                DraggableText tempShape = (DraggableText) selectedShape;
                if (tempShape.isLabel) {
                    backing = null;

                    backing = new tpsRemove(app, selectedShape);
                    selectedShape = null;

                    transact.addTransaction(backing);
                    gui.redoButton.setDisable(false);
                }
            } else {
                backing = null;

                backing = new tpsRemove(app, selectedShape);
                selectedShape = null;

                transact.addTransaction(backing);
                gui.redoButton.setDisable(false);
            }
        }
    }

    /**
     * Unused function from HW2
     */
    public void moveSelectedShapeToBack() {
        if (selectedShape != null) {
            shapes.remove(selectedShape);
            if (shapes.isEmpty()) {
                shapes.add(selectedShape);
            } else {
                ArrayList<Node> temp = new ArrayList<>();
                temp.add(selectedShape);
                for (Node node : shapes) {
                    temp.add(node);
                }
                shapes.clear();
                for (Node node : temp) {
                    shapes.add(node);
                }
            }
        }
    }

    /**
     * Unused function from HW2
     */
    public void moveSelectedShapeToFront() {
        if (selectedShape != null) {
            shapes.remove(selectedShape);
            shapes.add(selectedShape);
        }
    }

    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void resetData() {
        setState(SELECTING_SHAPE);
        newShape = null;
        selectedShape = null;

        // INIT THE COLORS
        currentFillColor = Color.web(WHITE_HEX);
        currentOutlineColor = Color.web(BLACK_HEX);

        shapes.clear();
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().clear();
    }

    /**
     * Resizes Selected Shape
     */
    public void selectSizedShape() {
        if (selectedShape != null) {
            unhighlightShape(selectedShape);
        }
        selectedShape = newShape;
        highlightShape(selectedShape);
        newShape = null;
        if (state == SIZING_SHAPE) {
            state = ((Draggable) selectedShape).getStartingState();
        }
    }

    /**
     * Unhighlights selected shape
     *
     * @param shape
     */
    public void unhighlightShape(Shape shape) {
        selectedShape.setEffect(null);
    }

    /**
     * Highlights selected shape
     *
     * @param shape
     */
    public void highlightShape(Shape shape) {
        shape.setEffect(highlightedEffect);
    }

    /**
     * Creates a rectangle object
     *
     * @param x
     * @param y
     */
    public void startNewRectangle(int x, int y) {
        DraggableRectangle newRectangle = new DraggableRectangle();
        newRectangle.start(x, y);
        newShape = newRectangle;
        initNewShape();

    }

    /**
     * Creates an ellipse object
     *
     * @param x
     * @param y
     */
    public void startNewEllipse(int x, int y) {
        DraggableEllipse newEllipse = new DraggableEllipse();
        newEllipse.start(x, y);
        newShape = newEllipse;
        initNewShape();
    }

    /**
     * Creates an image object
     *
     * @param x
     * @param y
     */
    public void startNewImage(int x, int y) {
        FileChooser fc = new FileChooser();

        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        fc.setInitialDirectory(new File(PATH_IMAGES));
        fc.setTitle("Image:");

        File selectedFile = fc.showOpenDialog(null);
        if (selectedFile != null) {
            String fuck = selectedFile.toURI().toString();
            DraggableImage newImage = new DraggableImage(fuck);
            newImage.start(x, y);
            newShape = newImage;
            if (selectedShape != null) {
                unhighlightShape(selectedShape);
                selectedShape = null;
            }

            backing = new tpsAdd(app, newShape);
            transact.addTransaction(backing);
        }

    }

    public void startNewImage(int x, int y, int width, int height, String path, boolean background) {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        DraggableImage prevShape = null;

        File selectedFile = new File(path);

        String fuck = selectedFile.toURI().toString();
        DraggableImage newImage = new DraggableImage(fuck);
        newImage.start(x, y);
        newShape = newImage;
        if (selectedShape != null) {
            unhighlightShape(selectedShape);
            selectedShape = null;
        }

        if (background) {
            newImage.start(0, 0);
            newImage.background = true;
            newImage.layoutXProperty().bind(workspace.getCanvas().widthProperty().subtract(workspace.getCanvas().widthProperty().divide(2).add(newImage.widthProperty().divide(2))));
            newImage.layoutYProperty().bind(workspace.getCanvas().heightProperty().subtract(workspace.getCanvas().heightProperty().divide(2).add(newImage.heightProperty().divide(2))));

            for (int i = 0; i < workspace.getCanvas().getChildren().size(); i++) {
                if (workspace.getCanvas().getChildren().get(i) instanceof DraggableImage) {
                    if (((DraggableImage) workspace.getCanvas().getChildren().get(i)).background == true) {
                        prevShape = ((DraggableImage) workspace.getCanvas().getChildren().get(i));
                        workspace.getCanvas().getChildren().remove(i);
                    }
                }
            }

            backing = new tpsBackgroundImage(app, newShape, prevShape);
            transact.addTransaction(backing);
        } else {
            backing = new tpsAdd(app, newShape);
            transact.addTransaction(backing);
        }
    }

    public void startNewBackground() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        DraggableImage prevShape = null;
        FileChooser fc = new FileChooser();

        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        fc.setInitialDirectory(new File(PATH_IMAGES));
        fc.setTitle("Image:");

        File selectedFile = fc.showOpenDialog(null);
        if (selectedFile != null) {
            String fuck = selectedFile.toURI().toString();
            DraggableImage newImage = new DraggableImage(fuck);
            newImage.start(0, 0);
            newImage.background = true;
            newImage.layoutXProperty().bind(workspace.getCanvas().widthProperty().subtract(workspace.getCanvas().widthProperty().divide(2).add(newImage.widthProperty().divide(2))));
            newImage.layoutYProperty().bind(workspace.getCanvas().heightProperty().subtract(workspace.getCanvas().heightProperty().divide(2).add(newImage.heightProperty().divide(2))));

            for (int i = 0; i < workspace.getCanvas().getChildren().size(); i++) {
                if (workspace.getCanvas().getChildren().get(i) instanceof DraggableImage) {
                    if (((DraggableImage) workspace.getCanvas().getChildren().get(i)).background == true) {
                        prevShape = ((DraggableImage) workspace.getCanvas().getChildren().get(i));
                        workspace.getCanvas().getChildren().remove(i);
                    }
                }
            }

            newShape = newImage;
            if (selectedShape != null) {
                unhighlightShape(selectedShape);
                selectedShape = null;
            }

            backing = new tpsBackgroundImage(app, newShape, prevShape);
            transact.addTransaction(backing);
        }
    }

    /**
     * Creates a new text object
     *
     * @param x
     * @param y
     */
    public void startNewText(int x, int y) {

        TextInputDialog text = new TextInputDialog("Text");
        text.setTitle("Text Input");
        text.setContentText("Text to add:");
        Optional<String> data = text.showAndWait();
        try {
            DraggableText newText = new DraggableText(data.get(), x, y);
            newShape = newText;
            if (selectedShape != null) {
                unhighlightShape(selectedShape);
                selectedShape = null;
            }

            mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
            newShape.setFill(workspace.getFontColorPicker().getValue());

            backing = new tpsAdd(app, newShape);
            transact.addTransaction(backing);
        } catch (NoSuchElementException e) {

        }

    }

    public void startNewText(int x, int y, String Text, Color fillColor, int fontSize, boolean bold, boolean italic, String fontFamily) {

        DraggableText newText = new DraggableText(Text, x, y);
        newText.setFill(fillColor);
        newText.setBold(bold);
        newText.setItalics(italic);

        if (bold && italic) {
            newText.setFont(Font.font(fontFamily, FontWeight.BOLD, FontPosture.ITALIC, fontSize));
        } else if (bold && !italic) {
            newText.setFont(Font.font(fontFamily, FontWeight.BOLD, FontPosture.REGULAR, fontSize));
        } else if (italic && bold) {
            newText.setFont(Font.font(fontFamily, FontWeight.NORMAL, FontPosture.ITALIC, fontSize));
        } else if (!bold && !italic) {
            newText.setFont(Font.font(fontFamily, FontWeight.NORMAL, FontPosture.REGULAR, fontSize));
        }

        newShape = newText;
        if (selectedShape != null) {
            unhighlightShape(selectedShape);
            selectedShape = null;
        }

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();

        backing = new tpsAdd(app, newShape);
        transact.addTransaction(backing);

    }

    /**
     * Initializes new shape
     */
    public void initNewShape() {
        // DESELECT THE SELECTED SHAPE IF THERE IS ONE
        if (selectedShape != null) {
            unhighlightShape(selectedShape);
            selectedShape = null;
        } else {
            selectedShape = newShape;
        }

        // USE THE CURRENT SETTINGS FOR THIS NEW SHAPE
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        if (newShape instanceof DraggableStation) {
            newShape.setFill(workspace.getStatColPicker().getValue());
            newShape.setStroke(workspace.getStatColPicker().getValue());

            DraggableStation newStation = (DraggableStation) newShape;

            newStation.setRadiusX(workspace.getStationThicknessSlider().getValue());
            newStation.setRadiusY(workspace.getStationThicknessSlider().getValue());
        } else if (newShape instanceof DraggableLine) {
            newShape.setStroke(workspace.getLineColorPicker().getValue());
            newShape.setStrokeWidth(workspace.getLineThicknessSlider().getValue());
        } else if (newShape instanceof DraggableText) {
            DraggableText tempText = (DraggableText) newShape;
            if (tempText.inLine) {
                newShape.setStroke(workspace.getLineColorPicker().getValue());
                DraggableText tempShape = (DraggableText) newShape;
                ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(tempShape.tempShape);
            } else if (tempText.inStation) {
                newShape.setStroke(workspace.getStatColPicker().getValue());
                DraggableText tempShape = (DraggableText) newShape;
                ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(tempShape.tempShape);
            } else {
                newShape.setStroke(workspace.getFontColorPicker().getValue());
                DraggableText tempShape = (DraggableText) newShape;
                ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(tempShape.tempShape);
            }
        } else {
            newShape.setFill(workspace.getFillColorPicker().getValue());
            newShape.setStroke(workspace.getOutlineColorPicker().getValue());
            newShape.setStrokeWidth(workspace.getOutlineThicknessSlider().getValue());
        }

        // ADD THE SHAPE TO THE CANVAS
        backing = new tpsAdd(app, newShape);
        transact.addTransaction(backing);

        // GO INTO SHAPE SIZING MODE
        if (newShape instanceof DraggableImage) {
        } else if (newShape instanceof DraggableStation) {
        } else if (newShape instanceof DraggableLine) {
        } else {
            state = mapState.SIZING_SHAPE;
        }
    }

    /**
     * Gets newest shape
     *
     * @return
     */
    public Shape getNewShape() {
        return newShape;
    }

    /**
     * Gets selected shape
     *
     * @return
     */
    public Shape getSelectedShape() {
        return selectedShape;
    }

    /**
     * Sets selected shape's shape
     *
     * @param initSelectedShape
     */
    public void setSelectedShape(Shape initSelectedShape) {
        selectedShape = initSelectedShape;
    }

    /**
     * Selects topmost shape
     *
     * @param x
     * @param y
     * @return
     */
    public Shape selectTopShape(int x, int y) {
        try {
            Shape shape = getTopShape(x, y);

            if (shape == selectedShape) {
                return shape;
            }

            if (selectedShape != null) {
                unhighlightShape(selectedShape);
            }
            if (shape != null) {
                highlightShape(shape);
                mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
                workspace.loadSelectedShapeSettings(shape);
            }
            selectedShape = shape;
            if (shape instanceof DraggableLine) {
                selectedLine = (DraggableLine) shape;
                mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
                workspace.lines.getSelectionModel().select(selectedLine.name);
            }

            if (shape != null) {
                ((Draggable) shape).startXY(x, y);
            }
            return shape;
        } catch (ClassCastException e) {
            return null;
        }
    }

    /**
     * Returns topmost shape
     *
     * @param x
     * @param y
     * @return
     */
    public Shape getTopShape(int x, int y) {
        for (int i = shapes.size() - 1; i >= 0; i--) {
            Shape shape = (Shape) shapes.get(i);
            if (shape.contains(x, y)) {
                return shape;
            }
        }
        return null;
    }

    /**
     * Adds Shape to canvas
     *
     * @param shapeToAdd
     */
    public void addShape(Shape shapeToAdd) {
        shapes.add(shapeToAdd);
    }

    /**
     * Removes Shape to canvas
     *
     * @param shapeToRemove
     */
    public void removeShape(Shape shapeToRemove) {
        shapes.remove(shapeToRemove);
    }

    /**
     * Gets current state
     *
     * @return
     */
    public mapState getState() {
        return state;
    }

    /**
     * sets current state
     *
     * @param initState
     */
    public void setState(mapState initState) {
        state = initState;
    }

    /**
     * Checks in state
     *
     * @param testState
     * @return
     */
    public boolean isInState(mapState testState) {
        return state == testState;
    }

    /**
     * Sets text to bold
     */
    public void processBold() {
        Shape shape = getSelectedShape();
        jTPS_Transaction t = new tpsBold(((DraggableText) shape), app);

        if (shape instanceof DraggableText) {

            DraggableText shapeText = (DraggableText) getSelectedShape();

            if (shapeText.getBold() && shapeText.getItalics()) {
                shapeText.setBold(false);
                shapeText.setFont(Font.font(shapeText.getFont().getFamily(), FontWeight.NORMAL, FontPosture.ITALIC, shapeText.getFont().getSize()));
            } else if (shapeText.getBold() && !shapeText.getItalics()) {
                shapeText.setFont(Font.font(shapeText.getFont().getFamily(), FontWeight.NORMAL, FontPosture.REGULAR, shapeText.getFont().getSize()));
                shapeText.setBold(false);
            } else if (!shapeText.getBold() && shapeText.getItalics()) {
                shapeText.setFont(Font.font(shapeText.getFont().getFamily(), FontWeight.BOLD, FontPosture.ITALIC, shapeText.getFont().getSize()));
                shapeText.setBold(true);
            } else if (!shapeText.getBold() && !shapeText.getItalics()) {
                shapeText.setFont(Font.font(shapeText.getFont().getFamily(), FontWeight.BOLD, FontPosture.REGULAR, shapeText.getFont().getSize()));
                shapeText.setBold(true);
            }

            transact.addTransaction(t);
        }
    }

    /**
     * Sets text to italics
     */
    public void processItalic() {
        Shape shape = getSelectedShape();

        if (shape instanceof DraggableText) {

            DraggableText shapeText = (DraggableText) getSelectedShape();

            jTPS_Transaction t = new tpsItalicizer((DraggableText) shapeText, app);
            if (shapeText.getBold() && shapeText.getItalics()) {
                shapeText.setItalics(false);
                shapeText.setFont(Font.font(shapeText.getFont().getFamily(), FontWeight.BOLD, FontPosture.REGULAR, shapeText.getFont().getSize()));
            } else if (shapeText.getBold() && !shapeText.getItalics()) {
                shapeText.setFont(Font.font(shapeText.getFont().getFamily(), FontWeight.BOLD, FontPosture.ITALIC, shapeText.getFont().getSize()));
                shapeText.setItalics(true);
            } else if (!shapeText.getBold() && shapeText.getItalics()) {
                shapeText.setFont(Font.font(shapeText.getFont().getFamily(), FontWeight.NORMAL, FontPosture.REGULAR, shapeText.getFont().getSize()));
                shapeText.setItalics(false);
            } else if (!shapeText.getBold() && !shapeText.getItalics()) {
                shapeText.setFont(Font.font(shapeText.getFont().getFamily(), FontWeight.NORMAL, FontPosture.ITALIC, shapeText.getFont().getSize()));
                shapeText.setItalics(true);
            }
            transact.addTransaction(t);
        }
    }

    /**
     * Sets texts fonts
     */
    public void processFonts() {
        Shape shape = getSelectedShape();
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();

        if (shape instanceof DraggableText) {
            String prevFontChoice = ((DraggableText) shape).getFont().getFamily();

            DraggableText shapeText = (DraggableText) getSelectedShape();
            String i = workspace.fonts.getSelectionModel().getSelectedItem();

            shapeText.setFont(Font.font(i, shapeText.getFont().getSize()));

            String curFontChoice = ((DraggableText) shape).getFont().getFamily();
            jTPS_Transaction fontChoice = new tpsTextFont(prevFontChoice, curFontChoice, app, ((DraggableText) shapeText));
            transact.addTransaction(fontChoice);
        }
    }

    /**
     * Sets font sizes
     */
    public void processSizes() {
        Shape shape = getSelectedShape();
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();

        if (shape instanceof DraggableText) {
            double prevFont = ((DraggableText) shape).getFont().getSize();
            DraggableText shapeText = (DraggableText) getSelectedShape();
            Integer i = workspace.sizes.getSelectionModel().getSelectedItem();
            shapeText.setFont(Font.font(shapeText.getFont().getFamily(), i));;
            double curFont = ((DraggableText) shape).getFont().getSize();
            jTPS_Transaction textEdit = new tpsTextSize(prevFont, curFont, app, ((DraggableText) shapeText));
            transact.addTransaction(textEdit);
        }
    }

    /**
     * Creates an Line object
     *
     * @param x
     * @param y
     * @param name
     */
    public void startNewLine(int x, int y, String name) {
        DraggableLine newLine = new DraggableLine(name);
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        newLine.start(x, y);

        newShape = newLine;
        newShape.setStroke(workspace.getLineColorPicker().getValue());
        newShape.setStrokeWidth(workspace.getLineThicknessSlider().getValue());
        setSelectedLine((DraggableLine) newShape);

        // ADD THE SHAPE TO THE CANVAS
        DraggableText newShape1 = newLine.startText;
        newShape1.setStroke(workspace.getLineColorPicker().getValue());
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(newShape1.tempShape);

        // ADD THE SHAPE TO THE CANVAS
        newShape1 = newLine.endText;
        newShape1.setStroke(workspace.getLineColorPicker().getValue());
        ((mapWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().add(newShape1.tempShape);

        if (selectedShape != null) {
            unhighlightShape(selectedShape);
            selectedShape = null;
        } else {
            selectedShape = newLine;
        }

        jTPS_Transaction textEdit = new tpsAddLine(app, newLine, newLine.startText, newLine.endText);
        transact.addTransaction(textEdit);

    }

    public void startNewLine(String name, boolean circular, Color fillColor, ArrayList<String> stations, int outlinethickness, int startX, int startY, int endX, int endY) {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();

        startNewLine(startX, startY, name);
        DraggableLine newLine = (DraggableLine) newShape;

        ((LineTo) newLine.getElements().get(newLine.getElements().size() - 1)).setX(endX);
        ((LineTo) newLine.getElements().get(newLine.getElements().size() - 1)).setY(endY);
        newLine.circular = circular;
        newLine.setStrokeWidth(outlinethickness);
        newLine.setStroke(fillColor);
        newShape = newLine;

        setSelectedLine((DraggableLine) newShape);
        // ADD THE SHAPE TO THE CANVAS
        newShape = newLine.startText;
        newShape.setStroke(fillColor);

        // ADD THE SHAPE TO THE CANVAS
        newShape = newLine.endText;
        newShape.setStroke(fillColor);

        for (int x = 0; x < stations.size(); x++) {
            Pane canvas = workspace.getCanvas();
            String check = stations.get(x);
            check = check.substring(1, check.length() - 1);
            for (int i = 0; i < canvas.getChildren().size(); i++) {
                if (canvas.getChildren().get(i) instanceof DraggableStation) {
                    DraggableStation line = (DraggableStation) canvas.getChildren().get(i);
                    if (line.name.equals(check)) {
                        newLine.addStations(line);
                    }
                }
            }
        }
    }

    /**
     * Creates an Station object
     *
     * @param x
     * @param y
     */
    public void startNewStation(int x, int y, String name) {
        DraggableStation newStation = new DraggableStation(name);
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        newStation.start(x, y);
        newShape = newStation;

        newShape.setFill(workspace.getStatColPicker().getValue());
        newShape.setStroke(workspace.getStatColPicker().getValue());

        newStation.setRadiusX(workspace.getStationThicknessSlider().getValue());
        newStation.setRadiusY(workspace.getStationThicknessSlider().getValue());

        setSelectedStation((DraggableStation) newShape);

        newShape = newStation.labelText;
        newShape.setStroke(workspace.getStatColPicker().getValue());
        newShape.setFill(workspace.getStatColPicker().getValue());

        newShape = newStation;

        backing = new tpsAddStat(app, newStation, newStation.labelText);
        transact.addTransaction(backing);
    }

    public void startNewStation(int x, int y, String name, int width, int height, Color fillColor, double rotate, int labelState) {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();

        startNewStation(x, y, name);
        DraggableStation newStation = (DraggableStation) newShape;
        newStation.setRadiusX(width / 2);
        newStation.setRadiusY(height / 2);
        newStation.setFill(fillColor);
        newStation.setStroke(fillColor);
        newStation.labelState = labelState;

        newStation.labelText.xProperty().unbind();
        newStation.labelText.yProperty().unbind();
        switch (newStation.labelState) {
            case 2:
                newStation.labelText.xProperty().bind(newStation.centerXProperty().add(-20.00));
                newStation.labelText.yProperty().bind(newStation.centerYProperty().add(20.00));
                break;
            case 3:
                newStation.labelText.xProperty().bind(newStation.centerXProperty().add(-20.00));
                newStation.labelText.yProperty().bind(newStation.centerYProperty().add(-20.00));
                break;
            case 4:
                newStation.labelText.xProperty().bind(newStation.centerXProperty().add(20.00));
                newStation.labelText.yProperty().bind(newStation.centerYProperty().add(-20.00));
                break;
            case 1:
                newStation.labelText.xProperty().bind(newStation.centerXProperty().add(20.00));
                newStation.labelText.yProperty().bind(newStation.centerYProperty().add(20.00));
                break;
        }

        newStation.setRadiusX(workspace.getStationThicknessSlider().getValue());
        newStation.setRadiusY(workspace.getStationThicknessSlider().getValue());

        newStation.labelText.setFill(fillColor);
        newStation.labelText.setStroke(fillColor);
        newStation.labelText.setRotate(rotate);
        newStation.rotate = rotate;

        setSelectedStation((DraggableStation) newShape);
    }

    public void editSelectedLine(Color selectedColor, String text) {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        String prevName = selectedLine.name;
        Color prevColor = (Color) selectedLine.getStroke();

        workspace.lines.getSelectionModel().clearSelection();
        reload();
        workspace.lineNames.remove(selectedLine.name);
        selectedLine.name = text;
        selectedLine.endText.setText(text);
        selectedLine.startText.setText(text);
        selectedLine.endText.setStroke(selectedColor);
        selectedLine.startText.setStroke(selectedColor);
        selectedLine.endText.setFill(selectedColor);
        selectedLine.startText.setFill(selectedColor);
        selectedLine.setStroke(selectedColor);
        workspace.lineNames.add(selectedLine.name);

        backing = new tpsEditLine(app, prevName, prevColor, text, selectedColor, selectedLine);
        transact.addTransaction(backing);
    }

    public void removeSelectedLine() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        if (selectedLine != null) {

            backing = null;

            jTPS_Transaction textEdit = new tpsRemoveLine(app, selectedLine, selectedLine.startText, selectedLine.endText);

            selectedShape = selectedLine.endText;
            workspace.getCanvas().getChildren().remove(selectedShape);

            selectedShape = selectedLine.startText;
            workspace.getCanvas().getChildren().remove(selectedShape);

            String temp = selectedLine.name;
            selectedShape = selectedLine;
            workspace.getCanvas().getChildren().remove(selectedShape);

            selectedLine = null;
            selectedShape = null;
            workspace.lineNames.remove(temp);

            transact.addTransaction(textEdit);
            reload();
            gui.redoButton.setDisable(false);
        }
    }

    public void reload() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.lines.getSelectionModel().clearSelection();
        workspace.statChoices.getSelectionModel().clearSelection();
        workspace.originStat.getSelectionModel().clearSelection();
        workspace.destStat.getSelectionModel().clearSelection();

        Pane canvas = workspace.getCanvas();
        for (int i = 0; i < canvas.getChildren().size(); i++) {
            if (canvas.getChildren().get(i) instanceof DraggableLine) {
                DraggableLine line = (DraggableLine) canvas.getChildren().get(i);
                if (workspace.lineNames.contains(line.name)) {

                } else if ((!workspace.lineNames.contains(line.name))) {
                    workspace.lineNames.add(line.name);
                }
            } else if (canvas.getChildren().get(i) instanceof DraggableStation) {
                DraggableStation line = (DraggableStation) canvas.getChildren().get(i);
                if (workspace.statNames.contains(line.name)) {

                } else if (!workspace.statNames.contains(line.name)) {
                    workspace.originStatName.add(line.name);
                    workspace.destStatName.add(line.name);
                    workspace.statNames.add(line.name);
                }
            }
        }
    }

    public void setSelectedLine() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();

        for (int i = 0; i < canvas.getChildren().size(); i++) {
            if (canvas.getChildren().get(i) instanceof DraggableLine) {
                DraggableLine line = (DraggableLine) canvas.getChildren().get(i);
                if (line.name == workspace.lines.getSelectionModel().getSelectedItem()) {
                    selectedLine = line;
                    selectedShape = line;
                }
            }
        }
    }

    public void setSelectedLine(DraggableLine line) {
        selectedLine = line;
        selectedShape = line;
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.lines.getSelectionModel().select(line.name);
    }

    public DraggableLine getSelectedLine() {
        return selectedLine;
    }

    public void removeStation() {
        if ((selectedStation != null) && (selectedLine != null)) {
            backing = new tpsRemoveStatInLine(app, selectedLine, selectedStation);
            transact.addTransaction(backing);
        }
    }

    public void addStation() {
        if ((selectedStation != null) && (selectedLine != null)) {
            backing = new tpsAddStatInLine(app, selectedLine, selectedStation);
            transact.addTransaction(backing);
        }
    }

    public void removeSelectedStation() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        ArrayList<DraggableLine> lines = new ArrayList<>();

        if (selectedStation != null) {

            backing = null;

            for (int i = 0; i < canvas.getChildren().size(); i++) {
                if (canvas.getChildren().get(i) instanceof DraggableLine) {
                    DraggableLine line = (DraggableLine) canvas.getChildren().get(i);
                    if (line.stations.contains(selectedStation)) {
                        line.removeStations(selectedStation);
                        lines.add(line);
                    }
                }
            }

            backing = new tpsRemoveStat(app, selectedStation, selectedStation.labelText, lines);
            selectedStation = null;

            transact.addTransaction(backing);
            reload();
            gui.redoButton.setDisable(false);
        }
    }

    public void setSelectedStation() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();

        for (int i = 0; i < canvas.getChildren().size(); i++) {
            if (canvas.getChildren().get(i) instanceof DraggableStation) {
                DraggableStation line = (DraggableStation) canvas.getChildren().get(i);
                if (line.name == workspace.statChoices.getSelectionModel().getSelectedItem()) {
                    selectedStation = line;
                    selectedShape = line;
                    workspace.getStationThicknessSlider().setValue(line.getRadiusX());
                }
            }
        }

    }

    public void setSelectedStation(DraggableStation line) {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.statChoices.getSelectionModel().select(line.name);
        workspace.getStationThicknessSlider().setValue(line.getRadiusX());
        setSelectedStation();
    }

    public DraggableStation getSelectedStation() {
        return selectedStation;
    }

    public void setStationThickness() {
        if (selectedStation != null) {
            mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
            double prevRadius = selectedStation.getRadiusX();
            selectedStation.setRadiusX(workspace.getStationThicknessSlider().getValue());
            selectedStation.setRadiusY(workspace.getStationThicknessSlider().getValue());
            double currentRadius = selectedStation.getRadiusX();
            backing = new tpsStationRadius(prevRadius, currentRadius, selectedStation, app);
            transact.addTransaction(backing);
        }
    }

    public void setLineThickness() {
        if (selectedLine != null) {
            double prevOutline = selectedLine.getStrokeWidth();
            mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
            selectedLine.setStrokeWidth(workspace.getLineThicknessSlider().getValue());
            double currentOutline = selectedLine.getStrokeWidth();
            backing = new tpsLineThickness(prevOutline, currentOutline, selectedLine, app);
            transact.addTransaction(backing);
        }
    }

    public void rotateLabel() {
        if (selectedStation != null) {
            double prevRotate = selectedStation.labelText.getRotate();
            switch ((int) selectedStation.labelText.getRotate()) {
                case 0:
                    selectedStation.labelText.setRotate(30.0);
                    break;
                case 30:
                    selectedStation.labelText.setRotate(60.0);
                    break;
                case 60:
                    selectedStation.labelText.setRotate(90.0);
                    break;
                case 90:
                    selectedStation.labelText.setRotate(120.0);
                    break;
                case 120:
                    selectedStation.labelText.setRotate(150.0);
                    break;
                case 150:
                    selectedStation.labelText.setRotate(180.0);
                    break;
                case 180:
                    selectedStation.labelText.setRotate(210.0);
                    break;
                case 210:
                    selectedStation.labelText.setRotate(240.0);
                    break;
                case 240:
                    selectedStation.labelText.setRotate(270.0);
                    break;
                case 270:
                    selectedStation.labelText.setRotate(300.0);
                    break;
                case 300:
                    selectedStation.labelText.setRotate(330.0);
                    break;
                case 330:
                    selectedStation.labelText.setRotate(0.0);
                    break;
            }
            selectedStation.rotate = selectedStation.labelText.getRotate();
            double currentRotate = selectedStation.labelText.getRotate();
            backing = new tpsRotate(prevRotate, currentRotate, selectedStation, app);
            transact.addTransaction(backing);
        }
    }

    public void snapCurrentStationToGrid() {
        if (selectedStation != null) {
            double x = selectedStation.getCenterX();
            double y = selectedStation.getCenterY();

            double prevX = selectedStation.getCenterX();
            double prevY = selectedStation.getCenterY();

            if (x % 25 <= 12) {
                x -= x % 25;
            } else {
                x += x % 25;
            }

            if (y % 25 > 12) {
                y -= y % 25;
            } else {
                y += y % 25;
            }

            selectedStation.setCenterX(x);
            selectedStation.setCenterY(y);

            double currentX = selectedStation.getCenterX();
            double currentY = selectedStation.getCenterY();
            backing = new tpsToggleSnap(prevX, prevY, currentX, currentY, selectedStation, app);
            transact.addTransaction(backing);
        }
    }

    public String findStat(DraggableStation originStat, DraggableStation destStat) {
        ArrayList<DraggableLine> lines = new ArrayList<>();
        String Path = "";
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();

        if (originStat.equals(destStat)) {
            return "Arrived";
        }
        
        for (int i = 0; i < canvas.getChildren().size(); i++) {
            if(canvas.getChildren().get(i) instanceof DraggableLine){
             lines.add((DraggableLine)canvas.getChildren().get(i));
            }
        }
        
        for (int i = 0; i < lines.size(); i++) {

            if(lines.get(i).stations.contains(originStat)){
                Path += "Station: " + lines.get(i).name + "\n";
                break;
            }

        }

        
        for (int i = 0; i < lines.size(); i++) {

            if(lines.get(i).stations.contains(destStat)){
                Path += "Go To:" + lines.get(i).name + "\n";
                break;
            }

        }

        return Path;
    }

    public void moveLabel() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();

        if (selectedStation != null) {
            DraggableStation stat = selectedStation;
            int prevState = stat.labelState;
            stat.labelText.xProperty().unbind();
            stat.labelText.yProperty().unbind();
            switch (stat.labelState) {
                case 1:
                    stat.labelText.xProperty().bind(stat.centerXProperty().add(-20.00));
                    stat.labelText.yProperty().bind(stat.centerYProperty().add(20.00));
                    stat.labelState = 2;
                    break;
                case 2:
                    stat.labelText.xProperty().bind(stat.centerXProperty().add(-20.00));
                    stat.labelText.yProperty().bind(stat.centerYProperty().add(-20.00));
                    stat.labelState = 3;
                    break;
                case 3:
                    stat.labelText.xProperty().bind(stat.centerXProperty().add(20.00));
                    stat.labelText.yProperty().bind(stat.centerYProperty().add(-20.00));
                    stat.labelState = 4;
                    break;
                case 4:
                    stat.labelText.xProperty().bind(stat.centerXProperty().add(20.00));
                    stat.labelText.yProperty().bind(stat.centerYProperty().add(20.00));
                    stat.labelState = 1;
                    break;
            }
            int currentState = stat.labelState;
            backing = new tpsMoveLabel(app, prevState, currentState, selectedStation);
            transact.addTransaction(backing);

        }
    }

}
