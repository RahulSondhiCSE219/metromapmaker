package map.gui;

import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.shape.Shape;
import map.data.mapData;
import map.data.Draggable;
import map.data.mapState;
import static map.data.mapState.DRAGGING_NOTHING;
import static map.data.mapState.DRAGGING_SHAPE;
import static map.data.mapState.SELECTING_SHAPE;
import static map.data.mapState.SIZING_SHAPE;
import djf.AppTemplate;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import map.data.DraggableText;
import map.tps.tpsText;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import map.data.DraggableLine;
import map.data.DraggableStation;
import static map.data.mapState.DRAGGING_LABEL;
import static map.data.mapState.DRAGGING_LABELDROP;

/**
 * This class responds to interactions with the rendering surface.
 *
 * @author Richard McKenna
 * @author Rahul Sondhi
 * @version 1.0
 */
public class CanvasController {

    AppTemplate app;
    jTPS transact;
    jTPS_Transaction backing;
    
    /**
     * Constructor to controller for canvas
     * @param initApp 
     */
    public CanvasController(AppTemplate initApp) {
        app = initApp;
    }

    /**
     * Respond to mouse presses on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMousePress(int x, int y) {
        mapData dataManager = (mapData) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            // SELECT THE TOP SHAPE
            Shape shape = dataManager.selectTopShape(x, y);
            Scene scene = app.getGUI().getPrimaryScene();

            // AND START DRAGGING IT
            if (shape != null) {
                scene.setCursor(Cursor.MOVE);
                Draggable shapeTemp = (Draggable) shape;
                
                if(shapeTemp instanceof DraggableLine){
                    dataManager.setSelectedLine((DraggableLine)shapeTemp);
                }else if (shapeTemp instanceof DraggableStation){
                    dataManager.setSelectedStation((DraggableStation)shapeTemp);
                }
                
                if(shapeTemp instanceof DraggableText){
                    DraggableText tempShape = (DraggableText)shapeTemp;
                    if(tempShape.inLine == true){
                        shapeTemp.startXY(x, y);
                        dataManager.setState(mapState.DRAGGING_SHAPE);
                        tempShape.startDrag(); 
                    }else if(tempShape.isLabel == true){
                        shapeTemp.startXY(x, y);
                        dataManager.setState(mapState.DRAGGING_SHAPE);
                    }else{
                        scene.setCursor(Cursor.DEFAULT);
                    }
                }else{
                    shapeTemp.startXY(x, y);
                    dataManager.setState(mapState.DRAGGING_SHAPE);
                }
                
                app.getGUI().updateToolbarControls(false);
            } else {
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }

        }else if (dataManager.isInState(DRAGGING_LABEL)){
            
        
            // SELECT THE TOP SHAPE
            Shape shape = dataManager.selectTopShape(x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            
            if (shape != null) {
                
                Draggable shapeTemp = (Draggable) shape;
                
            if(shapeTemp instanceof DraggableText){
                    DraggableText tempShape = (DraggableText)shapeTemp;
                    if(tempShape.inStation == true){
                        scene.setCursor(Cursor.MOVE);
                        tempShape.xProperty().unbind();
                        tempShape.yProperty().unbind();
                        shapeTemp.startXY(x, y);
                        dataManager.setState(mapState.DRAGGING_LABELDROP);
                    }
            }
           } else {
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
        
        
        }else if (dataManager.isInState(mapState.STARTING_RECTANGLE)) {
            dataManager.startNewRectangle(x, y);
            dataManager.setState(SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(mapState.STARTING_ELLIPSE)) {
            dataManager.startNewEllipse(x, y);
            dataManager.setState(SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(mapState.STARTING_IMAGE)) {
            dataManager.startNewImage(x, y);
            dataManager.setState(SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(mapState.STARTING_TEXT)) {
            dataManager.startNewText(x, y);
            dataManager.setState(SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
        }else if (dataManager.isInState(mapState.ADDING_STATLINE)) {
            // SELECT THE TOP SHAPE
            Shape shape = dataManager.selectTopShape(x, y);
            Scene scene = app.getGUI().getPrimaryScene();

            if (shape != null) {
                if(shape instanceof DraggableStation){
                    dataManager.setSelectedStation((DraggableStation)shape);
                    dataManager.addStation();
                }else{
                    dataManager.setState(SELECTING_SHAPE);
                    scene.setCursor(Cursor.DEFAULT);
                    app.getGUI().updateToolbarControls(false);  
                }
            }else{
                dataManager.setState(SELECTING_SHAPE);
                scene.setCursor(Cursor.DEFAULT);
                app.getGUI().updateToolbarControls(false);  
            }
            
        }else if (dataManager.isInState(mapState.REMOVING_STATLINE)) {
            
            // SELECT THE TOP SHAPE
            Shape shape = dataManager.selectTopShape(x, y);
            Scene scene = app.getGUI().getPrimaryScene();

            if (shape != null) {
                if(shape instanceof DraggableStation){
                    dataManager.setSelectedStation((DraggableStation)shape);
                    dataManager.removeStation();
                }else{
                    dataManager.setState(SELECTING_SHAPE);
                    scene.setCursor(Cursor.DEFAULT);
                    app.getGUI().updateToolbarControls(false);  
                }
            }else{
                dataManager.setState(SELECTING_SHAPE);
                scene.setCursor(Cursor.DEFAULT);
                app.getGUI().updateToolbarControls(false);  
            }
            
        } else if (dataManager.isInState(mapState.STARTING_LINE)) {
        
        Stage popupwindow = new Stage();

        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("Metro Map Maker - New Line");

        Label header = new Label("Metro Line Details");
        
        TextField lineName = new TextField();
        mapWorkspace workspace = (mapWorkspace)app.getWorkspaceComponent();
        ColorPicker colorpick = workspace.getLineColorPicker();
        
        HBox buttonlayout = new HBox(20);
        Button enter = new Button("Enter");
        Button cancel = new Button("Cancel");
        buttonlayout.getChildren().addAll(enter,cancel);
        
        enter.setOnAction(e -> {
            if(lineName.getText() != null && !(lineName.getText().isEmpty())){
                dataManager.startNewLine(x, y, lineName.getText());
                popupwindow.close();
            }
        });
        
        cancel.setOnAction(e -> {
           popupwindow.close();
        });
        
        VBox layout = new VBox(20);
        layout.getChildren().addAll(header,lineName,colorpick,buttonlayout);
        layout.setAlignment(Pos.CENTER);
        buttonlayout.setAlignment(Pos.CENTER);
        Scene sceneLanguage = new Scene(layout, 700, 416);

        popupwindow.setScene(sceneLanguage);

        popupwindow.showAndWait();
        
            dataManager.setState(SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
            
        }else if (dataManager.isInState(mapState.STARTING_STATION)) {
            
Stage popupwindow = new Stage();

        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("Metro Map Maker - New Station");

        Label header = new Label("Metro Station Details");
        
        TextField lineName = new TextField();
        
        HBox buttonlayout = new HBox(20);
        Button enter = new Button("Enter");
        Button cancel = new Button("Cancel");
        buttonlayout.getChildren().addAll(enter,cancel);
        
        enter.setOnAction(e -> {
            if(lineName.getText() != null && !(lineName.getText().isEmpty())){
                dataManager.startNewStation(x, y, lineName.getText());
                popupwindow.close();
            }
        });
        
        cancel.setOnAction(e -> {
           popupwindow.close();
        });
        
        VBox layout = new VBox(20);
        layout.getChildren().addAll(header,lineName,buttonlayout);
        layout.setAlignment(Pos.CENTER);
        buttonlayout.setAlignment(Pos.CENTER);
        Scene sceneLanguage = new Scene(layout, 700, 416);

        popupwindow.setScene(sceneLanguage);

        popupwindow.showAndWait();
        
            dataManager.setState(SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
        }
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMouseDragged(int x, int y) {
        mapData dataManager = (mapData) app.getDataComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
            Draggable newDraggableShape = (Draggable) dataManager.getNewShape();
            newDraggableShape.size(x, y);
        } else if (dataManager.isInState(DRAGGING_SHAPE)) {
            Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedShape();
            
            selectedDraggableShape.drag(x, y);
            
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(DRAGGING_LABELDROP)){
            Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedShape();
            
            selectedDraggableShape.drag(x, y);
            
            app.getGUI().updateToolbarControls(false);
        }
    }

    /**
     * Respond to mouse button release on the rendering surface, which we call
     * canvas, but is actually a Pane.
     */
    public void processCanvasMouseRelease(int x, int y) {
        mapData dataManager = (mapData) app.getDataComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
            dataManager.selectSizedShape();
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(mapState.DRAGGING_SHAPE)) {
            dataManager.setState(SELECTING_SHAPE);
            
            Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedShape();
            
            if(selectedDraggableShape instanceof DraggableText){
                DraggableText tempShape = (DraggableText)selectedDraggableShape;
                if(tempShape.inLine){
                   tempShape.endDrag(); 
                }
            }
            
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(DRAGGING_LABELDROP)){
            dataManager.setState(DRAGGING_LABEL);
            
            Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedShape();
            DraggableText tempShape = (DraggableText)selectedDraggableShape;
            
            DraggableStation station = dataManager.getSelectedStation();
            
            tempShape.xProperty().bind(station.centerXProperty().add((tempShape.xProperty().getValue() - station.centerXProperty().getValue())));
            tempShape.yProperty().bind(station.centerYProperty().add((tempShape.yProperty().getValue() - station.centerYProperty().getValue())));
            
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.CROSSHAIR);
            app.getGUI().updateToolbarControls(false);
        }else if (dataManager.isInState(mapState.DRAGGING_NOTHING)) {
            dataManager.setState(SELECTING_SHAPE);
        }
    }
    
    /**
     * Respond to mouse button double click on canvas
     * @param x
     * @param y
     * @param useApp 
     */
    void processCanvasDoubleMousePress(int x, int y, AppTemplate useApp) {
        mapData dataManager = (mapData) useApp.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            // SELECT THE TOP SHAPE
            Shape shape = dataManager.selectTopShape(x, y);
            Scene scene = useApp.getGUI().getPrimaryScene();

            if(shape!=null){// AND START DRAGGING IT
            if (shape instanceof DraggableText) {
                DraggableText tempShape = (DraggableText)shape;
                if(tempShape.inLine != true && tempShape.inStation != true){
                    transact = dataManager.getTransact();
                    String s = ((DraggableText) dataManager.getSelectedShape()).getText();
                    ((DraggableText) shape).setText();
                    String s2 = ((DraggableText) dataManager.getSelectedShape()).getText();
                    jTPS_Transaction backing = new tpsText(s, s2, app, ((DraggableText) dataManager.getSelectedShape()));
                    transact.addTransaction(backing);
                }
            }
            }
        }
    }
}
