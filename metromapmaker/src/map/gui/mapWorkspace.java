package map.gui;

import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import map.data.mapData;
import static map.data.mapData.BLACK_HEX;
import static map.data.mapData.WHITE_HEX;
import map.data.mapState;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static map.css.mapStyle.*;
import static map.metroLanguageProperty.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Ellipse;
import jtps.jTPS;
import map.data.DraggableLine;
import map.data.DraggableStation;
import properties_manager.PropertiesManager;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author Rahul Sondhi
 * @version 1.0
 */
public class mapWorkspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    jTPS transact;
    
    DraggableLine currentLine;
    DraggableStation currentStation;

    // HAS ALL THE CONTROLS FOR EDITING
    VBox editToolbar;

    VBox row1;
    BorderPane row1a;
    HBox row1b;
    HBox row1c;
    Label lineName;
    public ComboBox lines;
    public ObservableList lineNames = FXCollections.observableArrayList();
    ColorPicker lineColorPicker;
    String colorText;
    Button addStat;
    Button removeStat;
    Button removeLabel;
    Button addLine;
    Button removeLine;
    Button editLine;
    Button lineDetails;
    Slider lineThickness;

    VBox row2;
    BorderPane row2a;
    HBox row2b;
    HBox row2c;
    Label statName;
    public ComboBox statChoices;
    public ObservableList statNames = FXCollections.observableArrayList();
    String statColorText;
    ColorPicker statColPicker;
    Button editStation;
    Button addStation;
    Button removeStation;
    Button snapToGrid;
    Button moveLabel;
    Button rotateLabel;
    Slider stationThickness;

    BorderPane row3;
    VBox row3a;
    VBox row3b;
    public ComboBox originStat;
    public ObservableList originStatName = FXCollections.observableArrayList();
    public ObservableList destStatName = FXCollections.observableArrayList();
    public ComboBox destStat;
    String fromToString;
    Button fromToDetails;

    VBox row4;
    BorderPane row4a;
    HBox row4b;
    Label decorLabel;
    String backgroundColText;
    ColorPicker backgroundColPick;
    Button removeElement;
    Button addLabel;
    Button addImage;
    Button backgroundImage;

    VBox row5;
    BorderPane row5a;
    HBox row5b;
    Label fontLabel;
    String fontColText;
    ColorPicker fontColPick;
    Button italicButton;
    Button boldButton;
    public ComboBox<Integer> sizes;
    public ComboBox<String> fonts;

    VBox row6;
    BorderPane row6a;
    HBox row6a1;
    HBox row6b;
    Label navLabel;
    Label gridLabel;
    public CheckBox gridOnOff;
    Button zoomin;
    Button zoomout;
    Button increaseMapSize;
    Button decreaseMapSize;

    // THIS IS WHERE WE'LL RENDER OUR DRAWING, NOTE THAT WE
    // CALL THIS A CANVAS, BUT IT'S REALLY JUST A Pane
    Pane canvas;
    ScrollPane canvasHolder;

    // HERE ARE THE CONTROLLERS
    CanvasController canvasController;
    mapEditController mapEditController;

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;

    // FOR DISPLAYING DEBUG STUFF
    Text debugText;
    public String language;
    mapData dataManager;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public mapWorkspace(AppTemplate initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        transact = new jTPS();

        dataManager = (mapData) app.getDataComponent();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();

    }

    /**
     * Note that this is for displaying text during development.
     */
    public void setDebugText(String text) {
        debugText.setText(text);
    }

    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
    public ColorPicker getFillColorPicker() {
        return null;
    }

    public ColorPicker getOutlineColorPicker() {
        return null;
    }

    public ColorPicker getBackgroundColorPicker() {
        return backgroundColPick;
    }

    public ColorPicker getStatColPicker() {
        return statColPicker;
    }

    public ColorPicker getLineColorPicker() {
        return lineColorPicker;
    }

    public Slider getLineThicknessSlider() {
        return lineThickness;
    }

    public Slider getStationThicknessSlider() {
        return stationThickness;
    }
    
    public ColorPicker getFontColorPicker(){
        return fontColPick;
    }

    public Pane getCanvas() {
        return canvas;
    }
    
    public ScrollPane getCanvasHolder() {
        return canvasHolder;
    }

    public Slider getOutlineThicknessSlider() {
        return null;
    }

    /**
     * Function helps sets up layout
     */
    private void initLayout() {

        editToolbar = new VBox();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        row1 = new VBox();

        row1a = new BorderPane();
        lineName = new Label(props.getProperty(LINENAME_TITLE.toString()));
        lines = new ComboBox<>(lineNames);
        colorText = BLACK_HEX;

        editLine = gui.initChildButtonText(row1a, colorText, EDITOR_TOOLTIP.toString(), false);
        row1a.getChildren().clear();

        lineColorPicker = new ColorPicker(Color.valueOf(BLACK_HEX));

        row1a.setLeft(lineName);
        row1a.setCenter(lines);
        row1a.setRight(editLine);
        BorderPane.setMargin(lineName, new Insets(24, 24, 24, 24));
        BorderPane.setMargin(lines, new Insets(24, 24, 24, 24));
        BorderPane.setMargin(editLine, new Insets(24, 24, 24, 24));

        row1b = new HBox(20);
        addLine = gui.initChildButton(row1b, ADDLINE_ICON.toString(), ADDLINE_TOOLTIP.toString(), false);
        removeLine = gui.initChildButton(row1b, REMOVELINE_ICON.toString(), REMOVELINE_TOOLTIP.toString(), false);
        addStat = gui.initChildButton(row1b, ADDSTAT_ICON.toString(), ADDSTAT_TOOLTIP.toString(), false);
        removeStat = gui.initChildButton(row1b, REMOVESTAT_ICON.toString(), REMOVESTAT_TOOLTIP.toString(), false);
        lineDetails = gui.initChildButton(row1b, LINEDETAILS_ICON.toString(), LINEDETAILS_TOOLTIP.toString(), false);
        row1c = new HBox();
        lineThickness = new Slider(3, 6, 1);
        row1c.getChildren().addAll(lineThickness);
        row1.getChildren().addAll(row1a, row1b, row1c);

        row2 = new VBox();

        row2a = new BorderPane();
        statName = new Label(props.getProperty(STATNAME_TITLE.toString()));
        statChoices = new ComboBox<>(statNames);
        statColPicker = new ColorPicker(Color.valueOf(BLACK_HEX));
        row2a.setLeft(statName);
        row2a.setCenter(statChoices);
        row2a.setRight(statColPicker);
        BorderPane.setMargin(statName, new Insets(24, 24, 24, 24));
        BorderPane.setMargin(statChoices, new Insets(24, 24, 24, 24));
        BorderPane.setMargin(statColPicker, new Insets(24, 24, 24, 24));

        row2b = new HBox(20);
        addStation = gui.initChildButton(row2b, ADDSTATION_ICON.toString(), ADDSTATION_TOOLTIP.toString(), false);
        removeStation = gui.initChildButton(row2b, REMOVESTATION_ICON.toString(), REMOVESTATION_TOOLTIP.toString(), false);
        snapToGrid = gui.initChildButton(row2b, SNAPTOGRID_ICON.toString(), SNAPTOGRID_TOOLTIP.toString(), false);
        moveLabel = gui.initChildButton(row2b, MOVELABEL_ICON.toString(), MOVELABEL_TOOLTIP.toString(), false);
        rotateLabel = gui.initChildButton(row2b, ROTATELABEL_ICON.toString(), ROTATELABEL_TOOLTIP.toString(), false);
        row2c = new HBox();
        stationThickness = new Slider(5, 10, 1);
        row2c.getChildren().addAll(stationThickness);
        row2.getChildren().addAll(row2a, row2b, row2c);

        row3 = new BorderPane();
        row3a = new VBox(20);
        originStat = new ComboBox<>(originStatName);
        destStat = new ComboBox<>(destStatName);
        row3a.getChildren().addAll(originStat, destStat);
        row3b = new VBox();
        fromToDetails = gui.initChildButton(row3b, FINDROUTE_ICON.toString(), FINDROUTE_TOOLTIP.toString(), false);
        row3.setLeft(row3a);
        row3.setRight(row3b);
        BorderPane.setMargin(row3a, new Insets(24, 24, 24, 24));
        BorderPane.setMargin(row3b, new Insets(24, 24, 24, 24));

        row4 = new VBox();

        row4a = new BorderPane();
        decorLabel = new Label(props.getProperty(DECORLABEL_TITLE.toString()));
        backgroundColPick = new ColorPicker(Color.valueOf(WHITE_HEX));
        row4a.setLeft(decorLabel);
        row4a.setRight(backgroundColPick);
        BorderPane.setMargin(decorLabel, new Insets(24, 24, 24, 24));
        BorderPane.setMargin(backgroundColPick, new Insets(24, 24, 24, 24));

        row4b = new HBox(20);
        backgroundImage = gui.initChildButton(row4b, BACKGROUNDIMAGE_ICON.toString(), BACKGROUNDIMAGE_TOOLTIP.toString(), false);
        addImage = gui.initChildButton(row4b, ADDIMAGE_ICON.toString(), ADDIMAGE_TOOLTIP.toString(), false);
        addLabel = gui.initChildButton(row4b, ADDLABEL_ICON.toString(), ADDLABEL_TOOLTIP.toString(), false);
        removeElement = gui.initChildButton(row4b, REMOVEELEMENT_ICON.toString(), REMOVEELEMENT_TOOLTIP.toString(), false);
        row4.getChildren().addAll(row4a, row4b);

        row5 = new VBox();

        row5a = new BorderPane();
        fontLabel = new Label(props.getProperty(FONTLABEL_TITLE.toString()));
        fontColPick = new ColorPicker(Color.valueOf(BLACK_HEX));
        row5a.setLeft(fontLabel);
        row5a.setRight(fontColPick);
        BorderPane.setMargin(fontLabel, new Insets(24, 24, 24, 24));
        BorderPane.setMargin(fontColPick, new Insets(24, 24, 24, 24));

        row5b = new HBox(20);
        italicButton = gui.initChildButton(row5b, ITALIC_ICON.toString(), ITALIC_TOOLTIP.toString(), false);
        boldButton = gui.initChildButton(row5b, BOLD_ICON.toString(), BOLD_TOOLTIP.toString(), false);
        sizes = new ComboBox<>(FXCollections.observableArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 17, 19, 21, 24, 27, 33, 36, 40, 54, 72, 500));
        fonts = new ComboBox<>(FXCollections.observableArrayList("Times New Roman", "Courier New", "Arial", "Verdana", "Comic Sans MS"));

        fonts.setPromptText(props.getProperty(FONT_TOOLTIP.toString()));
        sizes.setPromptText(props.getProperty(SIZE_TOOLTIP.toString()));
        row5b.getChildren().addAll(fonts, sizes);
        row5.getChildren().addAll(row5a, row5b);

        row6 = new VBox();

        row6a = new BorderPane();
        navLabel = new Label(props.getProperty(NAVLABEL_TITLE.toString()));
        row6a1 = new HBox(20);
        gridLabel = new Label("Show Grid:");
        gridOnOff = new CheckBox();
        gridLabel.setTextFill(Color.web("#88D317"));
        row6a1.getChildren().addAll(gridLabel, gridOnOff);
        row6a.setLeft(navLabel);
        row6a.setRight(row6a1);
        BorderPane.setMargin(navLabel, new Insets(24, 24, 24, 24));
        BorderPane.setMargin(row6a1, new Insets(24, 24, 24, 24));

        row6b = new HBox(20);
        zoomin = gui.initChildButton(row6b, ZOOMIN_ICON.toString(), ZOOMIN_TOOLTIP.toString(), false);
        zoomout = gui.initChildButton(row6b, ZOOMOUT_ICON.toString(), ZOOMOUT_TOOLTIP.toString(), false);
        increaseMapSize = gui.initChildButton(row6b, INCREASEMAPSIZE_ICON.toString(), INCREASEMAPSIZE_TOOLTIP.toString(), false);
        decreaseMapSize = gui.initChildButton(row6b, DECREASEMAPSIZE_ICON.toString(), DECREASEMAPSIZE_TOOLTIP.toString(), false);
        row6.getChildren().addAll(row6a, row6b);

        row1.setAlignment(Pos.CENTER);
        row1b.setAlignment(Pos.CENTER);
        row1c.setAlignment(Pos.CENTER);
        row2.setAlignment(Pos.CENTER);
        row2b.setAlignment(Pos.CENTER);
        row2c.setAlignment(Pos.CENTER);
        row4.setAlignment(Pos.CENTER);
        row4b.setAlignment(Pos.CENTER);
        row5.setAlignment(Pos.CENTER);
        row5b.setAlignment(Pos.CENTER);
        row6.setAlignment(Pos.CENTER);
        row6b.setAlignment(Pos.CENTER);

        // NOW ORGANIZE THE EDIT TOOLBAR
        editToolbar.getChildren().add(row1);
        editToolbar.getChildren().add(row2);
        editToolbar.getChildren().add(row3);
        editToolbar.getChildren().add(row4);
        editToolbar.getChildren().add(row5);
        editToolbar.getChildren().add(row6);

        // WE'LL RENDER OUR STUFF HERE IN THE CANVAS
        canvas = new Pane();
        canvasHolder = new ScrollPane();
        canvasHolder.setContent(canvas);
        debugText = new Text();
        canvas.getChildren().add(debugText);
        debugText.setX(100);
        debugText.setY(100);
        

        // AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
        mapData data = (mapData) app.getDataComponent();
        data.setShapes(canvas.getChildren());

        // AND NOW SETUP THE WORKSPACE
        workspace = new BorderPane();
        ((BorderPane) workspace).setLeft(editToolbar);
        ((BorderPane) workspace).setCenter(canvasHolder);
        
        
        canvas.setPrefWidth(1240);
        canvas.setPrefHeight(930);
    }

    /**
     * Function helps setup functions linked to layout
     */
    private void initControllers() {
        // MAKE THE EDIT CONTROLLER
        mapEditController = new mapEditController(app);

        gui.undoButton.setOnAction(e -> {

            transact = dataManager.getTransact();

            transact.undoTransaction();

            app.getGUI().redoButton.setDisable(false);

            app.getGUI().updateToolbarControls(false);
        });

        gui.redoButton.setOnAction(e -> {

            transact = dataManager.getTransact();

            transact.doTransaction();

            app.getGUI().updateToolbarControls(false);
        });

        //*******************************************
        boldButton.setOnAction(e -> {
            mapEditController.processBold();
        });
        italicButton.setOnAction(e -> {
            mapEditController.processItalic();
        });
        fonts.setOnAction(e -> {
            mapEditController.processFonts();
        });
        sizes.setOnAction(e -> {
            mapEditController.processSizes();
        });
        
        fontColPick.setOnAction(e -> {
            mapEditController.processFontColors();
        });

        //*******************************************
        zoomin.setOnAction(e -> {
            mapEditController.processZoomin();
        });

        zoomout.setOnAction(e -> {
            mapEditController.processZoomout();
        });

        gridOnOff.setOnAction(e -> {
            mapEditController.processgridOnOff();
        });

        increaseMapSize.setOnAction(e -> {
            mapEditController.processIncreaseMapSize();
        });

        decreaseMapSize.setOnAction(e -> {
            mapEditController.processDecreaseMapSize();
        });
        
        canvasHolder.setOnKeyPressed(e -> {
            mapEditController.processKeyPress(e.getCode());
        });

        //*******************************************
        removeElement.setOnAction(e -> {
            mapEditController.processRemoveElement();
        });

        addLabel.setOnAction(e -> {
            mapEditController.processAddLabel();
        });

        addImage.setOnAction(e -> {
            mapEditController.processAddImage();
        });

        backgroundImage.setOnAction(e -> {
            mapEditController.processBackgroundImage();
        });

        backgroundColPick.setOnAction(e -> {
            mapEditController.processBackgroundColor();
        });

        //*******************************************
        fromToDetails.setOnAction(e -> {
            mapEditController.processFromToDetails();
        });

        //*******************************************
        rotateLabel.setOnAction(e -> {
            mapEditController.processRotateLabel();
        });

        moveLabel.setOnAction(e -> {
            mapEditController.processMoveLabel();
        });

        snapToGrid.setOnAction(e -> {
            mapEditController.processSnapToGrid();
        });

        removeStation.setOnAction(e -> {
            mapEditController.processRemoveStation();
        });

        addStation.setOnAction(e -> {
            mapEditController.processaddStation();
        });

        statChoices.setOnAction(e -> {
            mapEditController.processStatChoices();
        });

        stationThickness.valueProperty().addListener(e -> {
            mapEditController.processSelectStationThickness();
        });
        
        statColPicker.setOnAction(e -> {
            mapEditController.processStatColor();
        });

        //*******************************************
        lineDetails.setOnAction(e -> {
            mapEditController.processLineDetails();
        });

        editLine.setOnAction(e -> {
            mapEditController.processEditLine();
        });

        removeLine.setOnAction(e -> {
            mapEditController.processRemoveLine();
        });

        addLine.setOnAction(e -> {
            mapEditController.processAddLine();
        });

        removeStat.setOnAction(e -> {
            mapEditController.processRemoveStat();
        });

        addStat.setOnAction(e -> {
            mapEditController.processAddStat();
        });

        lines.setOnAction(e -> {
            mapEditController.processLines();
        });

        lineThickness.valueProperty().addListener(e -> {
            mapEditController.processSelectLineThickness();
        });

        //*******************************************
        canvasController = new CanvasController(app);
        canvas.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                canvasController.processCanvasDoubleMousePress((int) e.getX(), (int) e.getY(), app);
            }
        });
        canvas.setOnMousePressed(e -> {
            canvasController.processCanvasMousePress((int) e.getX(), (int) e.getY());
        });
        canvas.setOnMouseReleased(e -> {
            canvasController.processCanvasMouseRelease((int) e.getX(), (int) e.getY());
        });
        canvas.setOnMouseDragged(e -> {
            canvasController.processCanvasMouseDragged((int) e.getX(), (int) e.getY());
        });
    }

    /**
     * Function serves as a helper method for loading Shape Settings
     *
     * @param shape
     */
    public void loadSelectedShapeSettings(Shape shape) {
        if (shape != null) {

            if (shape.getFill() instanceof ImagePattern) {
            } else if (shape instanceof DraggableLine) {
                Color strokeColor = (Color) shape.getStroke();
                double lineThick = shape.getStrokeWidth();
                colorText = strokeColor.toString();
                lineColorPicker.setValue(strokeColor);
                lineThickness.setValue(lineThick);
                currentLine = (DraggableLine)shape;
                
                statNames.clear();
                for(int i = 0; i < currentLine.stations.size();i++){
                    statNames.add(currentLine.stations.get(i).name);
                }
                
            } else if (shape instanceof DraggableStation) {
                Color fillColor = (Color) shape.getFill();
                double lineThick = shape.getStrokeWidth();
                statColPicker.setValue(fillColor);
                stationThickness.setValue(lineThick);
                currentStation = (DraggableStation)shape;
            }
        }
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    public void initStyle() {
        // NOTE THAT EACH CLASS SHOULD CORRESPOND TO
        // A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
        // CSS FILE
        canvas.getStyleClass().add(CLASS_RENDER_CANVAS);

        // COLOR PICKER STYLE
        statColPicker.getStyleClass().add(CLASS_BUTTON);
//        lineColorPicker.getStyleClass().add(CLASS_BUTTON);
        backgroundColPick.getStyleClass().add(CLASS_BUTTON);
        fontColPick.getStyleClass().add(CLASS_BUTTON);

        editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        row1.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row2.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row3.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row4.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row5.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row6.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);

//        backgroundColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
//        fillColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
//        outlineColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
//        outlineThicknessLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
//
        lineName.setTextFill(Color.web("#88D317"));
        statName.setTextFill(Color.web("#88D317"));
        decorLabel.setTextFill(Color.web("#88D317"));
        fontLabel.setTextFill(Color.web("#88D317"));
        navLabel.setTextFill(Color.web("#88D317"));
        gridLabel.setTextFill(Color.web("#88D317"));
    }

    /**
     * This function reloads all the controls for editing logos the workspace.
     *
     * @param data
     */
    @Override
    public void reloadWorkspace(AppDataComponent data) {
        mapData dataManager = (mapData) data;
        


//       for(int i=0; i < canvas.getChildren().size(); i++){
//            if(canvas.getChildren().get(i) instanceof DraggableLine){
//                DraggableLine line = (DraggableLine)canvas.getChildren().get(i);
//                if(lineNames.contains(line.name)){
//
//                }else if((!lineNames.contains(line.name))){
//                    lineNames.add(line.name);
//                }
//            }else if(canvas.getChildren().get(i) instanceof DraggableStation){
//                   DraggableStation line = (DraggableStation)canvas.getChildren().get(i);
//                   if(statNames.contains(line.name)){
//                       
//                   }else if(!statNames.contains(line.name)){
//                    originStatName.add(line.name);
//                    destStatName.add(line.name);
//                    statNames.add(line.name);
//                   }
//            }
//        }
        
        removeElement.setDisable(dataManager.getSelectedShape() == null);
        backgroundColPick.setValue(dataManager.getBackgroundColor());
    }

    @Override
    public void resetWorkspace() {

    }

}
