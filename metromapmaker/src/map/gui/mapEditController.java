package map.gui;

import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;
import map.data.mapData;
import map.data.mapState;
import djf.AppTemplate;
import javafx.geometry.Pos;
import map.tps.tpsBackgroundColor;
import map.tps.tpsFillColor;
import map.tps.tpsLineThickness;
import map.tps.MoveBackToFront;
import map.tps.MoveFrontToBack;
import map.tps.tpsOutlineColor;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import static javafx.scene.paint.Color.BLACK;
import javafx.scene.shape.Line;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import map.data.DraggableLine;
import map.data.DraggableStation;
import map.data.DraggableText;
import map.tps.tpsDecreaseMapSize;
import map.tps.tpsFontColor;
import map.tps.tpsIncreaseMapSize;
import map.tps.tpsStatColor;
import map.tps.tpsZoomIn;
import map.tps.tpsZoomOut;

/**
 * This class responds to interactions with other UI logo editing controls.
 *
 * @author Richard McKenna
 * @author Rahul Sondhi
 * @version 1.0
 */
public class mapEditController {

    AppTemplate app;
    mapData dataManager;
    jTPS transact;
    jTPS_Transaction backing;
    boolean gridOn;

    /**
     * Constructor for controller
     *
     * @param initApp
     */
    public mapEditController(AppTemplate initApp) {
        app = initApp;
        dataManager = (mapData) app.getDataComponent();
        transact = new jTPS();
        gridOn = false;
    }

    /**
     * This method handles the response for selecting either the selection or
     * removal tool.
     */
    public void processSelectSelectionTool() {
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.DEFAULT);

        // CHANGE THE STATE
        dataManager.setState(mapState.SELECTING_SHAPE);

        // ENABLE/DISABLE THE PROPER BUTTONS
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    /**
     * This method handles a user request to remove the selected shape.
     */
    public void processRemoveSelectedShape() {
        // REMOVE THE SELECTED SHAPE IF THERE IS ONE
        dataManager.removeSelectedShape();

        // ENABLE/DISABLE THE PROPER BUTTONS
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    /**
     * This method processes a user request to start drawing a rectangle.
     */
    public void processSelectRectangleToDraw() {
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        // CHANGE THE STATE
        dataManager.setState(mapState.STARTING_RECTANGLE);

        // ENABLE/DISABLE THE PROPER BUTTONS
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    /**
     * This method provides a response to the user requesting to start drawing
     * an ellipse.
     */
    public void processSelectEllipseToDraw() {
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        // CHANGE THE STATE
        dataManager.setState(mapState.STARTING_ELLIPSE);

        // ENABLE/DISABLE THE PROPER BUTTONS
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    /**
     * This method processes a user request to move the selected shape down to
     * the back layer.
     */
    public void processMoveSelectedShapeToBack() {
        transact = dataManager.getTransact();
        backing = new MoveFrontToBack(app);
        dataManager.moveSelectedShapeToBack();
        transact.addTransaction(backing);
        app.getGUI().updateToolbarControls(false);
    }

    /**
     * This method processes a user request to move the selected shape up to the
     * front layer.
     */
    public void processMoveSelectedShapeToFront() {
        transact = dataManager.getTransact();
        backing = new MoveBackToFront(app);
        dataManager.moveSelectedShapeToFront();
        transact.addTransaction(backing);
        app.getGUI().updateToolbarControls(false);
    }

    /**
     * This method processes a user request to select a fill color for a shape.
     */
    public void processSelectFillColor() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getFillColorPicker().getValue();
        if (selectedColor != null) {
            Color prevColor = dataManager.getCurrentFillColor();
            dataManager.setCurrentFillColor(selectedColor);
            app.getGUI().updateToolbarControls(false);
            transact = dataManager.getTransact();
            backing = new tpsFillColor(prevColor, selectedColor, app);
            transact.addTransaction(backing);
        }
    }

    /**
     * This method processes a user request to select the outline color for a
     * shape.
     */
    public void processSelectOutlineColor() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getOutlineColorPicker().getValue();
        if (selectedColor != null) {
            Color prevColor = dataManager.getCurrentOutlineColor();
            dataManager.setCurrentOutlineColor(selectedColor);
            backing = new tpsOutlineColor(prevColor, selectedColor, app);
            transact.addTransaction(backing);
            app.getGUI().updateToolbarControls(false);

        }
    }

    /**
     * This method processes a user request to select the background color.
     */
    public void processSelectBackgroundColor() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getBackgroundColorPicker().getValue();
        if (selectedColor != null) {
            Color prevColor = dataManager.getBackgroundColor();
            dataManager.setBackgroundColor(selectedColor);
            app.getGUI().updateToolbarControls(false);

            transact = dataManager.getTransact();
            backing = new tpsBackgroundColor(prevColor, selectedColor, app);
            transact.addTransaction(backing);
        }
    }

    /**
     * This method processes a user request to select the outline thickness for
     * shape drawing.
     */
    public void processSelectOutlineThickness() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        int prevThickness = (int) dataManager.getCurrentBorderWidth();
        int outlineThickness = (int) workspace.getOutlineThicknessSlider().getValue();
        dataManager.setCurrentOutlineThickness(outlineThickness);
        app.getGUI().updateToolbarControls(false);
    }

    /**
     * This method processes a user request to take a snapshot of the current
     * scene.
     */
    public void processSnapshot(String path) {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
        File file = new File(path);
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Processes Image to add to canvas
     */
    void processImageToAdd() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        dataManager.setState(mapState.STARTING_IMAGE);

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    /**
     * Processes text to add to canvas
     */
    void processTextToAdd() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        dataManager.setState(mapState.STARTING_TEXT);

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    /**
     * Processes italic change for text
     */
    void processItalic() {
        dataManager.processItalic();
        app.getGUI().updateToolbarControls(false);
    }

    /**
     * Processes bold change for text
     */
    void processBold() {
        dataManager.processBold();
        app.getGUI().updateToolbarControls(false);
    }

    /**
     * Processes fonts change for text
     */
    void processFonts() {
        dataManager.processFonts();
        app.getGUI().updateToolbarControls(false);
    }

    /**
     * Processes size change for text
     */
    void processSizes() {
        dataManager.processSizes();
        app.getGUI().updateToolbarControls(false);
    }

    void processFontColors() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getFontColorPicker().getValue();
        if (selectedColor != null) {
            Color prevColor = dataManager.getCurrentFillColor();
            dataManager.setCurrentFillColor(selectedColor);
            app.getGUI().updateToolbarControls(false);
            transact = dataManager.getTransact();
            backing = new tpsFontColor(prevColor, selectedColor,(DraggableText)dataManager.getSelectedShape(),app);
            transact.addTransaction(backing);
        }
    }

//    void processCutShape() {
//        processCopyShape();
//        processRemoveSelectedShape();
//        
//        mapWorkspace workspace = (mapWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//    }
//
//    void processCopyShape() {
//        dataManager.moveCopyShape();
//        
//        mapWorkspace workspace = (mapWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//    }
//
//    void processPasteShape() {
//       dataManager.movePasteShape();
//       
//        mapWorkspace workspace = (mapWorkspace)app.getWorkspaceComponent();
//	workspace.reloadWorkspace(dataManager);
//    }
    void processZoomin() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        double prevScale = workspace.getCanvas().getScaleX();
        workspace.getCanvas().setScaleX(workspace.getCanvas().getScaleX() / 1.1);
        workspace.getCanvas().setScaleY(workspace.getCanvas().getScaleY() / 1.1);
        double currentScale = workspace.getCanvas().getScaleX();
        transact = dataManager.getTransact();
        backing = new tpsZoomIn(app,prevScale,currentScale);
        transact.addTransaction(backing);
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    void processZoomout() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        double prevScale = workspace.getCanvas().getScaleX();
        workspace.getCanvas().setScaleX(workspace.getCanvas().getScaleX() * 1.1);
        workspace.getCanvas().setScaleY(workspace.getCanvas().getScaleY() * 1.1);
        double currentScale = workspace.getCanvas().getScaleX();
        transact = dataManager.getTransact();
        backing = new tpsZoomOut(app,prevScale,currentScale);
        transact.addTransaction(backing);
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    void processgridOnOff() {

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        boolean on = gridOn;
        
        if (!on) {
            Pane gridShow = new Pane();
            double x = workspace.getCanvas().getPrefWidth();
            double y = workspace.getCanvas().getPrefHeight();

            x -= x % 25;
            y -= y % 25;

            gridShow.setPrefHeight(y);
            gridShow.setPrefWidth(x);

            
            workspace.getCanvas().getChildren().add(0, gridShow);

            while (y > 0) {
                while (x > 0) {
                    Line grid = new Line();
                    double height = workspace.getCanvas().getHeight();
                    grid.startXProperty().set(x);
                    grid.startYProperty().set(height);
                    grid.endXProperty().set(x);
                    grid.endYProperty().set(0);
                    grid.setStrokeWidth(3);
                    grid.setStroke(BLACK);
                    gridShow.getChildren().add(grid);
                    x = x - 25;
                }
                x = workspace.getCanvas().getWidth();
                Line grid = new Line();
                grid.startXProperty().set(0);
                grid.startYProperty().set(y);
                grid.endXProperty().set(x);
                grid.endYProperty().set(y);
                grid.setStrokeWidth(3);
                grid.setStroke(BLACK);
                gridShow.getChildren().add(grid);
                x -= x % 25;
                y = y - 25;
            }
            gridShow.setPickOnBounds(false);
            gridOn = true;
        } else {
            for (int i = 0; i < workspace.getCanvas().getChildren().size(); i++) {
                if (workspace.getCanvas().getChildren().get(i) instanceof Pane) {
                    workspace.getCanvas().getChildren().remove(i);
                }
            }
            gridOn = false;
        }
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    void processIncreaseMapSize() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        double prevHeight = workspace.getCanvas().getPrefHeight();
        double prevWidth = workspace.getCanvas().getPrefWidth();
        workspace.getCanvas().setPrefWidth(workspace.getCanvas().getPrefWidth() * 1.1);
        workspace.getCanvas().setPrefHeight(workspace.getCanvas().getPrefHeight() * 1.1);
        double currentHeight = workspace.getCanvas().getPrefHeight();
        double currentWidth = workspace.getCanvas().getPrefWidth();
        transact = dataManager.getTransact();
        backing = new tpsIncreaseMapSize(app,prevWidth,prevHeight,currentWidth,currentHeight);
        transact.addTransaction(backing);
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    void processDecreaseMapSize() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        double prevHeight = workspace.getCanvas().getPrefHeight();
        double prevWidth = workspace.getCanvas().getPrefWidth();
        workspace.getCanvas().setPrefWidth(workspace.getCanvas().getPrefWidth() / 1.1);
        workspace.getCanvas().setPrefHeight(workspace.getCanvas().getPrefHeight() / 1.1);
        double currentHeight = workspace.getCanvas().getPrefHeight();
        double currentWidth = workspace.getCanvas().getPrefWidth();
        transact = dataManager.getTransact();
        backing = new tpsDecreaseMapSize(app,prevWidth,prevHeight,currentWidth,currentHeight);
        transact.addTransaction(backing);
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    void processKeyPress(KeyCode fuck) {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();

        System.out.print("KeyStroke: ");
        switch (fuck) {
            case W:
                System.out.println("W");
                workspace.getCanvas().setTranslateY(workspace.getCanvas().getTranslateY() + 0.1 * workspace.getCanvas().getHeight());
                break;
            case A:
                System.out.println("A");
                workspace.getCanvas().setTranslateX(workspace.getCanvas().getTranslateX() + 0.1 * workspace.getCanvas().getWidth());
                break;
            case S:
                System.out.println("S");
                workspace.getCanvas().setTranslateY(workspace.getCanvas().getTranslateY() - 0.1 * workspace.getCanvas().getHeight());
                break;
            case D:
                System.out.println("D");
                workspace.getCanvas().setTranslateX(workspace.getCanvas().getTranslateX() - 0.1 * workspace.getCanvas().getWidth());
                break;
            case UP:
                System.out.println("W");
                workspace.getCanvas().setTranslateY(workspace.getCanvas().getTranslateY() + 0.1 * workspace.getCanvas().getHeight());
                break;
            case LEFT:
                System.out.println("A");
                workspace.getCanvas().setTranslateX(workspace.getCanvas().getTranslateX() + 0.1 * workspace.getCanvas().getWidth());
                break;
            case DOWN:
                System.out.println("S");
                workspace.getCanvas().setTranslateY(workspace.getCanvas().getTranslateY() - 0.1 * workspace.getCanvas().getHeight());
                break;
            case RIGHT:
                System.out.println("D");
                workspace.getCanvas().setTranslateX(workspace.getCanvas().getTranslateX() - 0.1 * workspace.getCanvas().getWidth());
                break;
        }

        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    //*******************************************
    void processRemoveElement() {
        dataManager.removeSelectedShape();

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    void processAddLabel() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        dataManager.setState(mapState.STARTING_TEXT);

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    void processBackgroundImage() {
        Stage popupwindow = new Stage();

        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("Metro Map Maker - Background Image");

        Label header = new Label("Alert Adding Background Image!");

        HBox buttonlayout = new HBox(20);
        Button enter = new Button("Enter");
        Button cancel = new Button("Cancel");
        buttonlayout.getChildren().addAll(enter, cancel);

        enter.setOnAction(e -> {
            dataManager.startNewBackground();
            popupwindow.close();
        });

        cancel.setOnAction(e -> {
            popupwindow.close();
        });

        VBox layout = new VBox(20);
        layout.getChildren().addAll(header, buttonlayout);
        layout.setAlignment(Pos.CENTER);
        buttonlayout.setAlignment(Pos.CENTER);
        Scene sceneLanguage = new Scene(layout, 300, 300);

        popupwindow.setScene(sceneLanguage);

        popupwindow.showAndWait();
    }

    void processAddImage() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        dataManager.setState(mapState.STARTING_IMAGE);

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    void processBackgroundColor() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getBackgroundColorPicker().getValue();
        if (selectedColor != null) {
            Color prevColor = dataManager.getBackgroundColor();
            dataManager.setBackgroundColor(selectedColor);
            app.getGUI().updateToolbarControls(false);

            transact = dataManager.getTransact();
            backing = new tpsBackgroundColor(prevColor, selectedColor, app);
            transact.addTransaction(backing);
        }
    }

    //*******************************************
    void processFromToDetails() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        DraggableStation originStat = null;
        DraggableStation destStat = null;

        for (int i = 0; i < canvas.getChildren().size(); i++) {
            if (canvas.getChildren().get(i) instanceof DraggableStation) {
                DraggableStation line = (DraggableStation) canvas.getChildren().get(i);
                if (line.name == workspace.originStat.getSelectionModel().getSelectedItem()) {
                    originStat = line;
                }
                if (line.name == workspace.destStat.getSelectionModel().getSelectedItem()) {
                    destStat = line;
                }
            }
        }
        
        
        if (originStat != null && destStat != null) {
            Stage popupwindow = new Stage();

            popupwindow.initModality(Modality.APPLICATION_MODAL);
            popupwindow.setTitle("Metro Map Maker - Metro Line Path");

            Label header = new Label("Path");

            TextArea lineName = new TextArea();
            lineName.setEditable(false);
            DraggableLine shape = dataManager.getSelectedLine();
            
                lineName.setText(dataManager.findStat(originStat, destStat));

            HBox buttonlayout = new HBox(20);
            Button enter = new Button("Enter");
            buttonlayout.getChildren().addAll(enter);

            enter.setOnAction(e -> {
                popupwindow.close();
            });

            VBox layout = new VBox(20);
            layout.getChildren().addAll(header, lineName, buttonlayout);
            layout.setAlignment(Pos.CENTER);
            buttonlayout.setAlignment(Pos.CENTER);
            Scene sceneLanguage = new Scene(layout, 400, 400);

            popupwindow.setScene(sceneLanguage);

            popupwindow.showAndWait();
        }

        // ENABLE/DISABLE THE PROPER BUTTONS
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    //*******************************************
    void processRotateLabel() {
        dataManager.rotateLabel();

        // ENABLE/DISABLE THE PROPER BUTTONS
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    void processMoveLabel() {
        dataManager.moveLabel();

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    void processSnapToGrid() {

        dataManager.snapCurrentStationToGrid();

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    void processRemoveStation() {
        if (dataManager.getSelectedStation() != null) {
            Stage popupwindow = new Stage();

            popupwindow.initModality(Modality.APPLICATION_MODAL);
            popupwindow.setTitle("Metro Map Maker - Remove Station");

            Label header = new Label("Alert Removing Metro Station!");

            HBox buttonlayout = new HBox(20);
            Button enter = new Button("Enter");
            Button cancel = new Button("Cancel");
            buttonlayout.getChildren().addAll(enter, cancel);

            enter.setOnAction(e -> {
                mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
                // REMOVE THE SELECTED SHAPE IF THERE IS ONE
                dataManager.removeSelectedStation();

                // ENABLE/DISABLE THE PROPER BUTTONS
                workspace.reloadWorkspace(dataManager);
                app.getGUI().updateToolbarControls(false);
                popupwindow.close();
            });

            cancel.setOnAction(e -> {
                popupwindow.close();
            });

            VBox layout = new VBox(20);
            layout.getChildren().addAll(header, buttonlayout);
            layout.setAlignment(Pos.CENTER);
            buttonlayout.setAlignment(Pos.CENTER);
            Scene sceneLanguage = new Scene(layout, 200, 200);

            popupwindow.setScene(sceneLanguage);

            popupwindow.showAndWait();
        }
    }

    void processaddStation() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        dataManager.setState(mapState.STARTING_STATION);

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    void processStatChoices() {
        dataManager.setSelectedStation();

        // ENABLE/DISABLE THE PROPER BUTTONS
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    void processSelectStationThickness() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
//	int prevThickness = (int)dataManager.getCurrentBorderWidth();
//        int outlineThickness = (int)workspace.getStationThicknessSlider().getValue();
        dataManager.setStationThickness();
        app.getGUI().updateToolbarControls(false);
//        transact = dataManager.getTransact();
//        backing = new tpsLineThickness(prevThickness, outlineThickness, app);
//        transact.addTransaction(backing);
    }

    //*******************************************
    void processLineDetails() {
        if (dataManager.getSelectedLine() != null) {
            Stage popupwindow = new Stage();

            popupwindow.initModality(Modality.APPLICATION_MODAL);
            popupwindow.setTitle("Metro Map Maker - Metro Line Stops");

            Label header = new Label("Metro Line Stops");

            TextArea lineName = new TextArea();
            lineName.setEditable(false);
            DraggableLine shape = dataManager.getSelectedLine();
            int i = 0;
            for (DraggableStation station : shape.stations) {
                i++;
                lineName.setText(lineName.getText() + "-> " + station.name + "\n");
                lineName.setPrefRowCount(i);
            }

            mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();

            HBox buttonlayout = new HBox(20);
            Button enter = new Button("Enter");
            buttonlayout.getChildren().addAll(enter);

            enter.setOnAction(e -> {
                popupwindow.close();
            });

            VBox layout = new VBox(20);
            layout.getChildren().addAll(header, lineName, buttonlayout);
            layout.setAlignment(Pos.CENTER);
            buttonlayout.setAlignment(Pos.CENTER);
            Scene sceneLanguage = new Scene(layout, 400, 400);

            popupwindow.setScene(sceneLanguage);

            popupwindow.showAndWait();
        }
    }

    void processSelectLineThickness() {
        //	int prevThickness = (int)dataManager.getCurrentBorderWidth();
//        int outlineThickness = (int)workspace.getStationThicknessSlider().getValue();
        dataManager.setLineThickness();
        app.getGUI().updateToolbarControls(false);
//        transact = dataManager.getTransact();
//        backing = new tpsLineThickness(prevThickness, outlineThickness, app);
//        transact.addTransaction(backing);
    }

    void processEditLine() {
        if (dataManager.getSelectedLine() != null) {
            Stage popupwindow = new Stage();

            popupwindow.initModality(Modality.APPLICATION_MODAL);
            popupwindow.setTitle("Metro Map Maker - Edit Line");

            Label header = new Label("Metro Line Details");

            TextField lineName = new TextField();
            mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
            DraggableLine shape = dataManager.getSelectedLine();
            lineName.setText(shape.name);
            ColorPicker colorpick = workspace.getLineColorPicker();
            colorpick.setValue((Color) shape.getStroke());

            HBox buttonlayout = new HBox(20);
            Button enter = new Button("Enter");
            Button cancel = new Button("Cancel");
            buttonlayout.getChildren().addAll(enter, cancel);

            enter.setOnAction(e -> {

                Color selectedColor = workspace.getLineColorPicker().getValue();
                if ((selectedColor != null && ((lineName.getText() != dataManager.getSelectedLine().name) || (selectedColor != dataManager.getSelectedLine().getStroke())))) {
                    dataManager.editSelectedLine(selectedColor, lineName.getText());

                    // ENABLE/DISABLE THE PROPER BUTTONS
                    workspace.reloadWorkspace(dataManager);
                    app.getGUI().updateToolbarControls(false);
                    popupwindow.close();
                } else {
                    popupwindow.close();
                }

            });

            cancel.setOnAction(e -> {
                popupwindow.close();
            });

            VBox layout = new VBox(20);
            layout.getChildren().addAll(header, lineName, colorpick, buttonlayout);
            layout.setAlignment(Pos.CENTER);
            buttonlayout.setAlignment(Pos.CENTER);
            Scene sceneLanguage = new Scene(layout, 700, 416);

            popupwindow.setScene(sceneLanguage);

            popupwindow.showAndWait();
        }
    }

    void processRemoveLine() {
        if (dataManager.getSelectedLine() != null) {
            Stage popupwindow = new Stage();

            popupwindow.initModality(Modality.APPLICATION_MODAL);
            popupwindow.setTitle("Metro Map Maker - Remove Line");

            Label header = new Label("Alert Removing Metro Line!");

            HBox buttonlayout = new HBox(20);
            Button enter = new Button("Enter");
            Button cancel = new Button("Cancel");
            buttonlayout.getChildren().addAll(enter, cancel);

            enter.setOnAction(e -> {
                mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
                // REMOVE THE SELECTED SHAPE IF THERE IS ONE
                dataManager.removeSelectedLine();

                // ENABLE/DISABLE THE PROPER BUTTONS
                workspace.reloadWorkspace(dataManager);
                app.getGUI().updateToolbarControls(false);
                popupwindow.close();
            });

            cancel.setOnAction(e -> {
                popupwindow.close();
            });

            VBox layout = new VBox(20);
            layout.getChildren().addAll(header, buttonlayout);
            layout.setAlignment(Pos.CENTER);
            buttonlayout.setAlignment(Pos.CENTER);
            Scene sceneLanguage = new Scene(layout, 200, 200);

            popupwindow.setScene(sceneLanguage);

            popupwindow.showAndWait();
        }
    }

    void processAddLine() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        dataManager.setState(mapState.STARTING_LINE);

        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    void processRemoveStat() {
        if (dataManager.getSelectedLine() != null) {
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.CROSSHAIR);

            dataManager.setState(mapState.REMOVING_STATLINE);

            mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
            workspace.reloadWorkspace(dataManager);
        }
    }

    void processAddStat() {
        if (dataManager.getSelectedLine() != null) {
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.CROSSHAIR);

            dataManager.setState(mapState.ADDING_STATLINE);

            mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
            workspace.reloadWorkspace(dataManager);
        }
    }

    void processLines() {
        dataManager.setSelectedLine();

        // ENABLE/DISABLE THE PROPER BUTTONS
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    void processStatColor() {
        mapWorkspace workspace = (mapWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getStatColPicker().getValue();
        if (selectedColor != null) {
            Color prevColor = (Color)dataManager.getSelectedStation().getFill();
            dataManager.setCurrentFillColor(selectedColor);
            app.getGUI().updateToolbarControls(false);
            transact = dataManager.getTransact();
            backing = new tpsStatColor(app, prevColor, selectedColor,dataManager.getSelectedStation());
            transact.addTransaction(backing);
        }
    }

}
